<?php
// Text
$_['text_subject']  = 'Te han enviado una Gift Card de %s';
$_['text_greeting'] = 'Felicitaciones, usted ha recibido un certificado de regalo por %s';
$_['text_from']     = 'Este regalo ha sido enviado a usted por %s';
$_['text_message']  = 'Con un mensaje que dice';
$_['text_redeem']   = 'Para utilizar esta Gift Card, anote el código de canje que es <b>%s</b> luego haga clic en el el enlace de más abajo y compre producto que desee utilizar con esta Gift Card. Puede introducir el código de su Gift Card en la página de carro de compras antes de terminar su compra.';
$_['text_footer']   = 'Por favor, responda a este correo electrónico si usted tiene alguna pregunta.';
