<?php
// Heading
$_['heading_title']	   = 'Registro de Errores';

// Text
$_['text_success']	   = 'Éxito: Ha limpiado su registro de errores!';
$_['text_list']        = 'Listado de Errores';

// Error
$_['error_warning']	   = 'Advertencia: El archivo de log de error %s es %s!';
$_['error_permission'] = 'Advertencia: No tienes permiso para borrar el registro de error!';