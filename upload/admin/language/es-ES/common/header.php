<?php
// Heading
$_['heading_title']        = 'OpenCart';

// Text
$_['text_order']           = 'Pedidos';
$_['text_order_status']    = 'pendiente';
$_['text_complete_status'] = 'Terminado';
$_['text_customer']        = 'Clientes';
$_['text_online']          = 'Clientes en línea';
$_['text_approval']        = 'Pendiente de aprobación';
$_['text_product']         = 'Productos';
$_['text_stock']           = 'Sin existencias';
$_['text_review']          = 'Comentarios';
$_['text_return']          = 'Devoluciones';
$_['text_affiliate']       = 'Afiliados';
$_['text_store']           = 'Tiendas';
$_['text_front']           = 'Tienda';
$_['text_help']            = 'Ayuda';
$_['text_homepage']        = 'Página de inicio';
$_['text_support']         = 'Foro de asistencia';
$_['text_documentation']   = 'Documentación';
$_['text_logout']          = 'Desconexión';