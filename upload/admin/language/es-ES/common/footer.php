<?php
/**
 * @version		$Id: NAME.xxx 2014-10-9 17:57Z mic $
 * @package		Translation Project
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Text
$_['text_footer'] 	= '<a href="http://www.opencart.com" target="_blank"> OpenCart</a> © 2009-2014 todos los derechos reservados.';
$_['text_version'] 	= 'Versión %s';