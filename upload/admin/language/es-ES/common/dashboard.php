<?php
// Heading
$_['heading_title']                = 'Panel de Control';

// Text
$_['text_order_total']             = 'Pedidos';
$_['text_customer_total']          = 'Clientes';
$_['text_sale_total']              = 'Ventas';
$_['text_online_total']            = 'Usuarios Online';
$_['text_map']                     = 'Mapa del Mundo';
$_['text_sale']                    = 'Análisis de ventas';
$_['text_activity']                = 'Actividad Reciente';
$_['text_recent']                  = 'Últimos pedidos';
$_['text_order']                   = 'Pedidos';
$_['text_customer']                = 'Clientes';
$_['text_day']                     = 'Hoy';
$_['text_week']                    = 'Semana';
$_['text_month']                   = 'Mes';
$_['text_year']                    = 'Año';
$_['text_view']                    = 'Ver más...';

// Error
$_['error_install']                = 'ADVERTENCIA: La carpeta /install/ todavía existe en su servidor y debe ser eliminada por razones de seguridad';