<?php
// header
$_['heading_title']  = 'Restablece tu contraseña';

// Text
$_['text_reset']     = 'Restablece tu contraseña!';
$_['text_password']  = 'Introduzca la nueva contraseña que desea utilizar.';
$_['text_success']   = 'Su contraseña ha sido actualizada satisfactoriamente.';

// Entry
$_['entry_password'] = 'Contraseña';
$_['entry_confirm']  = 'Confirmar';

// Error
$_['error_password'] = 'La contraseña debe tener entre 3 y 20 caracteres!';
$_['error_confirm']  = 'La contraseña y su confirmación no coinciden!';