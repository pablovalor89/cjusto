<?php
// Heading
$_['heading_title']         		= 'Amazon USA';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Dashboard de Amazon USA';

// Text
$_['text_heading_settings'] 		= 'Configuración';
$_['text_heading_account'] 			= 'Cambiar Plan';
$_['text_heading_links'] 			= 'Enlaces del Elemento';
$_['text_heading_register'] 		= 'Registro';
$_['text_heading_bulk_listing'] 	= 'Listado masivo';
$_['text_heading_stock_updates'] 	= 'Actualización de Stocks';
$_['text_heading_saved_listings'] 	= 'Listados guardados';
$_['text_heading_bulk_linking'] 	= 'Vinculado masivo';