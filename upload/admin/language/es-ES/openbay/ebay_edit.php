<?php
// Heading
$_['heading_title']					= 'Revisar anuncio de eBay';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Text
$_['text_revise']               	= 'Revisar la lista';
$_['text_loading']                  = 'Obtener información de artículos de eBay';
$_['text_error_loading']            = 'Hubo un error al obtener la información de eBay';
$_['text_saved']                    = 'Listado se ha guardado';
$_['text_alert_removed']            = 'El anuncio ha sido desvinculado';
$_['text_alert_ended']              = 'El anuncio ha sido terminado en eBay';

// Buttons
$_['button_view']					= 'Ver lista';
$_['button_remove']					= 'Quitar enlace';
$_['button_end']                    = 'lista Fin';
$_['button_retry']					= 'Reintentar';

// Entry
$_['entry_title']					= 'Título';
$_['entry_price']					= 'Precio de venta (incluyendo impuestos)';
$_['entry_stock_store']				= 'valores local';
$_['entry_stock_listed']			= 'acciones de eBay';
$_['entry_stock_reserve']			= 'nivel de la Reserva';
$_['entry_stock_matrix_active']		= 'Matriz de stock (activo)';
$_['entry_stock_matrix_inactive']	= 'Matriz de stock (inactivo)';

// Column
$_['column_sku']					= 'Código var / SKU';
$_['column_stock_listed']			= 'Listado';
$_['column_stock_reserve']			= 'Reserva';
$_['column_stock_total']			= 'En Stock';
$_['column_price']					= 'Precio';
$_['column_status']					= 'Activo';
$_['column_add']					= 'Añadir';
$_['column_combination']			= 'Combinación';

// Help
$_['help_stock_store']				= 'Este es el nivel de stock en OpenCart';
$_['help_stock_listed']				= 'Este es el nivel actual del stock en eBay';
$_['help_stock_reserve']			= 'Este es el nivel máximo de stock en eBay (0 no = límite de reserva)';

// Error
$_['error_ended']					= 'El anuncio vinculado ha finalizado, sin poder editarlo. Se debe eliminar el enlace.';
$_['error_reserve']					= 'No puede establecer la reserva superior a la población local';