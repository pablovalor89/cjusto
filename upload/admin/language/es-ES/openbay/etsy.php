<?php
// Heading
$_['heading_title']         		= 'Etsy';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_dashboard']				= 'Tablero Etsy';

// Messages
$_['text_success']         			= 'Has guardado tus cambios en la extensión Etsy';
$_['text_heading_settings']         = 'Configuración';
$_['text_heading_sync']             = 'Sincronizar';
$_['text_heading_register']         = 'Regístrese aquí';
$_['text_heading_products']        	= 'Enlaces del Elemento';
$_['text_heading_listings']        	= 'Listados de Etsy';

// Errors
$_['error_generic_fail']			= 'Se ha producido un Error desconocido!';
$_['error_permission']				= 'No tienes permiso para las configuraciones Etsy';