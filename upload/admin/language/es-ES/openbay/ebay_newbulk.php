<?php
//Heading
$_['text_page_title']               = 'Listado masivo';
$_['text_ebay']               		= 'eBay';
$_['text_openbay']               	= 'Openbay Pro';

// Buttons
$_['text_none']                     = 'Ningúno';
$_['text_preview']                  = 'Previsualizar';
$_['text_add']                      = 'Añadir';
$_['text_preview_all']              = 'Verificar todos';
$_['text_submit']                   = 'Enviar';
$_['text_features']                 = 'Caracteristicas';
$_['text_catalog']                  = 'Seleccionar catalogo';
$_['text_catalog_search']           = 'Catálogo de búsqueda';
$_['text_search_term']           	= 'Términos de búsqueda';
$_['text_close']           			= 'Cerrar';

//Form options / text
$_['text_pixels']                   = 'Píxeles';
$_['text_other']                    = 'Otro';

//Profile names
$_['text_profile']            		= 'Perfiles';
$_['text_profile_theme']            = 'Perfil de tema';
$_['text_profile_shipping']         = 'Perfil de envío';
$_['text_profile_returns']          = 'Volver al perfil';
$_['text_profile_generic']          = 'Perfil genérico';

//Text
$_['text_title']                    = 'Título';
$_['text_price']                    = 'Precio';
$_['text_stock']                    = 'Stock';
$_['text_search']                   = 'Buscar';
$_['text_loading']                  = 'Cargando detalles';
$_['text_preparing0']               = 'Preparando';
$_['text_preparing1']               = 'de';
$_['text_preparing2']               = 'elementos';
$_['entry_condition']                = 'Estado';
$_['text_duration']                 = 'Duración';
$_['text_category']                 = 'Categoría';
$_['text_exists']                   = 'Algunos elementos aparecen ya en eBay por lo que se han eliminado';
$_['text_error_count']              = 'Ha seleccionado %s elementos, puede tomar un tiempo procesar sus datos';
$_['text_verifying']                = 'Verificación de elementos';
$_['text_processing']               = 'Procesamiento <span id="activeItems"></span> artículos';
$_['text_listed']                   = 'Listado de artículo! ID: ';
$_['text_ajax_confirm_listing']     = '¿Es usted seguro de que desea a una lista de estos artículos?';
$_['text_bulk_plan_error']          = 'Su plan actual no permite cargas masivas, mejore su plan <a href="%s">aquí</a>';
$_['text_item_limit']               = 'Usted no puede enumerar estos elementos como excede el límite del plan, actualiza tu plan <a href="%s"> aquí</a>';
$_['text_search_text']              = 'Ingrese algún texto de búsqueda';
$_['text_catalog_no_products']      = 'No se encontraron elementos del catalogo';
$_['text_search_failed']            = 'Búsqueda fallida';
$_['text_esc_key']                  = 'La pagina de bienvenida se ha ocultado pero puede que no haya terminado de cargarse';
$_['text_loading_categories']       = 'Cargando categorías';
$_['text_loading_condition']        = 'Cargando condiciones del producto';
$_['text_loading_duration']         = 'Cargando duraciones de listado';
$_['text_total_fee']         		= 'Cuota total';
$_['text_category_choose']         	= 'Encontrar categoría';
$_['text_suggested']         		= 'Categorías sugeridas';

//Errors
$_['text_error_ship_profile']       = 'Es necesario tener un perfil de envío por defecto establecido';
$_['text_error_generic_profile']    = 'Es necesario tener un perfil genérico por defecto establecido';
$_['text_error_return_profile']     = 'Es necesario tener un perfil de retorno por defecto establecido';
$_['text_error_theme_profile']      = 'Es necesario tener un perfil de tema por defecto establecido';
$_['text_error_variants']           = 'Los elementos con variaciones no pueden ser masivamente listados y han sido deseleccionados';
$_['text_error_stock']              = 'Algunos artículos no están en stock y han sido removidos';
$_['text_error_no_product']         = 'No hay productos elegibles seleccionados para usar la característica de carga masiva';
$_['text_error_reverify']           = 'Ha ocurrido un error, debe editar y volver a verificar los elementos';
$_['error_missing_settings']   = 'No puede listar masivamente hasta que sincronice sus configuraciones de eBay';
$_['text_error_no_selection']   	= 'Debe seleccionar al menos 1 elemento a la lista';