<?php
// Heading
$_['heading_title']         		= 'Enlaces del artículo';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Buttons
$_['button_resync']               	= 'Re-sincronizar';
$_['button_check_unlinked']       	= 'Verifica artículos desligados';
$_['button_remove_link']       		= 'Quitar enlace';

// Errors
$_['error_ajax_load']          		= 'Disculpa, no podemos obtener una respuesta, intenta mas tarde.';
$_['error_validation']         		= 'Necesita registrarse para su token API y activar el módulo.';
$_['error_no_listings']      		= 'No se encontrarron productos ligados';
$_['error_link_value']          	= 'La liga al producto no es un valor';
$_['error_link_no_stock']    		= 'La liga no puede ser creada para un producto fuera de stock. Finalice el artículo manualmente en eBay.';
$_['error_subtract_setting']        = 'Éste producto está activado para no sustraer stock en OpenCart.';

// Text
$_['text_linked_items']             = 'Artículos enlazados';
$_['text_unlinked_items']           = 'Artículos no enlazados';
$_['text_alert_stock_local']        = 'Sus listados de eBay serán actualizados con su nivel de stock local!';
$_['text_link_desc1']               = 'Ligar tus artículos permitirá tener control de stock en tus anuncios de eBay.';
$_['text_link_desc2']               = 'Para cada elemento que se actualiza el stock local (el stock disponible en tu tienda OpenCart) actualizará tu anuncio de eBay';
$_['text_link_desc3']               = 'Su stock local es el stock disponible para venta. Su stock de eBay debe coincidir con esto.';
$_['text_link_desc4']               = 'Tu stock asignado son los articulos ya vendidos que no han sido pagados todavía. Estos artículos serán apartados y no calculados en tus niveles de stock disponible.';
$_['text_text_linked_desc']         = 'Los elementos vinculados son artículos de OpenCart que tienen un vínculo a un anuncio de eBay.';
$_['text_text_unlinked_desc']       = 'Los artículos desligados son artículos de su cuenta de eBay que no están ligados a ningún producto en OpenCart.';
$_['text_text_unlinked_info']       = 'Haz click en el botón de marcar artículos desligados para buscar artículos desligados en su listado de artículos de eBay. Esto puede tomar mucho tiempo si tiene muchos artículos listados en eBay.';
$_['text_text_loading_items']       = 'Cargando artículos';
$_['text_failed']       			= 'Error al cargar';
$_['text_limit_reached']       		= 'Se alcanzó el número máximo de busqueda por petición, haga clic en el botón para continuar la búsqueda';
$_['text_stock_error']       		= 'Error de stock';
$_['text_listing_ended']       		= 'Artículo finalizado';
$_['text_filter']             		= 'Filtrar resultados';
$_['text_filter_title']             = 'Título';
$_['text_filter_range']             = 'Rango de stock';
$_['text_filter_range_from']        = 'Min';
$_['text_filter_range_to']          = 'Máx';
$_['text_filter_var']             	= 'Incluye variaciones';

// Tables
$_['column_action']            		= 'Acción';
$_['column_status']            		= 'Estado';
$_['column_variants']          		= 'Variantes';
$_['column_item_id']            	= 'Identificador de producto de eBay';
$_['column_product']           		= 'Producto';
$_['column_product_auto']      		= 'Nombre del producto (auto completar)';
$_['column_listing_title']     		= 'Título del listado de eBay';
$_['column_allocated']         		= 'Stock asignado';
$_['column_ebay_stock']        		= 'acciones de eBay';
$_['column_stock_available']   		= 'Stock de tienda';
$_['column_stock_reserve']   		= 'nivel de la Reserva';