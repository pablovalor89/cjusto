<?php
// Heading
$_['heading_title']        			= 'Configuración del mercado';
$_['text_openbay']					= 'OpenBay Pro';
$_['text_ebay']						= 'eBay';

// Text
$_['text_developer']				= 'Desarrollador / soporte';
$_['text_app_settings']				= 'Configuración de la aplicación';
$_['text_default_import']			= 'Importar configuración predeterminada';
$_['text_payments']					= 'Pagos';
$_['text_notify_settings']			= 'Configuración de notificaciones';
$_['text_listing']					= 'Valores predeterminados de listados';
$_['text_token_register']			= 'Registrarse para su ficha de eBay';
$_['text_token_renew']				= 'Renueve su ficha de eBay';
$_['text_application_settings']		= 'La configuración de la aplicación permite configurar la forma de OpenBay Pro funciona y se integra con su sistema.';
$_['text_import_description']		= 'Personaliza el estado de un pedido durante las diferentes etapas. Se puede utilizar un estado de un pedido de eBay que no existe en esta lista.';
$_['text_payments_description']		= 'Complete sus opciones de pago para nuevas listas, esto le ahorrará ingresarlas por cada nueva lista que cree.';
$_['text_allocate_1']				= 'Cuando el cliente compra';
$_['text_allocate_2']				= 'Cuando el cliente había pagado';
$_['text_developer_description']	= 'No debería usar esta área a menos que se le indique hacerlo';
$_['text_payment_paypal']			= 'PayPal aceptado';
$_['text_payment_paypal_add']		= 'Dirección de email de Paypal';
$_['text_payment_cheque']			= 'Cheque aceptado';
$_['text_payment_card']				= 'Tarjetas aceptadas';
$_['text_payment_desc']				= 'Ver Descripción (por ejemplo, transferencia bancaria)';
$_['text_tax_use_listing'] 			= 'Usar tasa de impuestos establecidos en la lista de eBay';
$_['text_tax_use_value']			= 'Use un valor establecido para todo';
$_['text_action_warning']			= 'Esta acción es peligrosa por lo tanto la contraseña está protegida.';
$_['text_notifications']			= 'Controle cuando los clientes reciben notificaciones de la aplicación. La habilitación de correos electrónicos puede mejorar sus calificaciones de DSR ya que el usuario obtendrá actualizaciones sobre su pedido.';
$_['text_listing_1day']             = 'un día';
$_['text_listing_3day']             = '3 días';
$_['text_listing_5day']             = '5 días';
$_['text_listing_7day']             = '7 días';
$_['text_listing_10day']            = '10 días';
$_['text_listing_30day']            = '30 días';
$_['text_listing_gtc']              = 'Bueno hasta cancelar';
$_['text_api_status']               = 'Estado de la conexión del API';
$_['text_api_ok']                   = 'Conexión OK, el token caduca';
$_['text_api_failed']               = 'Validación fallida';
$_['text_api_other']        		= 'Otras acciones';
$_['text_create_date_0']            = 'Cuando se agrega a OpenCart';
$_['text_create_date_1']            = 'Cuando fue creado en eBay';
$_['text_obp_detail_update']        = 'Actualiza la URL de tu tienda &amp; Email de contacto';
$_['text_success']					= 'Su configuración ha sido guardada';

// Entry
$_['entry_status']					= 'Estado';
$_['entry_token']					= 'Token';
$_['entry_secret']					= 'Secreto';
$_['entry_string1']					= 'Cadena de cifrado 1';
$_['entry_string2']					= 'Cadena de cifrado 2';
$_['entry_end_items']				= '¿Finalizar artículos?';
$_['entry_relist_items']			= '¿Relistar cuando esté disponible?';
$_['entry_disable_soldout']			= '¿Desactivar el producto cuando no esté disponible?';
$_['entry_debug']					= 'Habilitar registro';
$_['entry_currency']				= 'Moneda por defecto';
$_['entry_customer_group']			= 'Grupo de clientes';
$_['entry_stock_allocate']			= 'Stock asignado';
$_['entry_created_hours']			= 'Nueva orden de la edad limite';
$_['entry_empty_data']				= '¿Vaciar TODOS los datos?';
$_['entry_developer_locks']			= '¿Quitar bloqueos de pedidos?';
$_['entry_payment_instruction']		= 'Instrucciones de pago';
$_['entry_payment_immediate']		= 'Requiere pago inmediato';
$_['entry_payment_types']			= 'Tipos de pago';
$_['entry_brand_disable']			= 'Des-habilitar enlace de marca';
$_['entry_duration']				= 'Duración predeterminada de la lista';
$_['entry_measurement']				= 'Sistema de medición';
$_['entry_address_format']			= 'Formato de dirección por defecto';
$_['entry_timezone_offset']			= 'Establecer zona horaria';
$_['entry_tax_listing']				= 'Impuesto del producto';
$_['entry_tax']						= 'Porcentaje de impuesto utilizado para todo';
$_['entry_create_date']				= 'Fecha de creación para nuevos pedidos';
$_['entry_password_prompt']			= 'Por favor ingrese su contraseña de administrador';
$_['entry_notify_order_update']		= 'Actualizaciones de pedidos';
$_['entry_notify_buyer']			= 'Nueva orden - comprador';
$_['entry_notify_admin']			= 'Nueva orden - Administrador';
$_['entry_import_pending']			= 'Importar pedidos pendientes de pago:';
$_['entry_import_def_id']			= 'Importar estado predeterminado:';
$_['entry_import_paid_id']			= 'Status pagado:';
$_['entry_import_shipped_id']		= 'Status del envío:';
$_['entry_import_cancelled_id']		= 'Status de cancelado:';
$_['entry_import_refund_id']		= 'Estado reembolsado:';
$_['entry_import_part_refund_id']	= 'Estado parcialmente reembolsado:';

// Tabs
$_['tab_api_info']					= 'Detalles de la API';
$_['tab_setup']						= 'Configuración';
$_['tab_defaults']					= 'Valores predeterminados de listados';

// Help
$_['help_disable_soldout']			= 'Cuando el articulo se vende, des-habilita el producto en OpenCart';
$_['help_relist_items'] 			= 'Si existía un enlace de elemento antes, volverá a mostrar el artículo anterior si vuelve a estar en stock';
$_['help_end_items']    			= 'Si los artículos se agotan, ¿debería finalizar el listado en eBay?';
$_['help_currency']     			= 'En función de las monedas de su tienda';
$_['help_created_hours']   			= 'Los pedidos son nuevos cuando son mas jóvenes que este limite (en horas). El valor predeterminado es 72';
$_['help_stock_allocate'] 			= '¿cuando se deben asignar las existencias de la tienda?';
$_['help_payment_instruction']  	= 'Sé lo más descriptivo posible. ¿Requiere un pago dentro de un cierto tiempo? ¿Llaman para pagar la tarjeta? ¿Tiene alguna forma de pago especial?';
$_['help_payment_immediate'] 		= 'El pago inmediato detiene a los compradores impagos, ya que un articulo no se vende hasta que paguen.';
$_['help_listing_tax']     			= 'Si usa la tarifa de los listados, asegúrese de que sus artículos tengan el impuesto correcto en eBay';
$_['help_tax']             			= 'Se usa cuando importa artículos o pedidos';
$_['help_duration']    				= 'GTC solo está disponible si tienes una tienda de eBay.';
$_['help_address_format']      		= 'Solo se usa si el país ya no tiene una configuración de formato de dirección.';
$_['help_create_date']         		= 'Elija qué tiempo creado aparecerá en un pedido cuando se lo importe';
$_['help_timezone_offset']     		= 'Basado en horas. 0 es la zona horaria GTM. Solo funciona si la hora de eBay se usa para crear pedidos.';
$_['help_notify_admin']   			= 'Notifique al administrador de la tienda con el nuevo correo electrónico de pedido';
$_['help_notify_order_update']		= 'Esto es para las actualizaciones automatizadas, por ejemplo, si actualiza un pedido en eBay, el nuevo estado se actualiza automáticamente en tu tienda.';
$_['help_notify_buyer']        		= 'Notifique al usuario con el nuevo correo electrónico de pedido predeterminado';
$_['help_measurement']        		= 'Elegir qué sistema de medición que desea utilizar para los listados';

// Buttons
$_['button_update']             	= 'Actualizar';
$_['button_repair_links']    		= 'Reparar enlaces del artículo';

// Error
$_['error_api_connect']         	= 'Error al conectar a la API';