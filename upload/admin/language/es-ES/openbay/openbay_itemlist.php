<?php
// Heading
$_['heading_title'] 					= 'Administra anuncios';

// Text
$_['text_markets']                  	= 'Mercados';
$_['text_openbay']                  	= 'OpenBay Pro';
$_['text_ebay'] 						= 'eBay';
$_['text_amazon'] 						= 'Amazon EU';
$_['text_amazonus'] 					= 'Amazon US';
$_['text_etsy'] 						= 'Etsy';
$_['text_status_all'] 					= 'Todo';
$_['text_status_ebay_active'] 			= 'eBay activo';
$_['text_status_ebay_inactive'] 		= 'eBay no activo';
$_['text_status_amazoneu_saved'] 		= 'Amazon EU guardado';
$_['text_status_amazoneu_processing'] 	= 'Amazon EU procesando';
$_['text_status_amazoneu_active'] 		= 'Amazon EU activo';
$_['text_status_amazoneu_notlisted'] 	= 'Amazon estados unidos no esta incluido';
$_['text_status_amazoneu_failed'] 		= 'Amazon EU fallido';
$_['text_status_amazoneu_linked'] 		= 'Amazon EU vinculado';
$_['text_status_amazoneu_notlinked'] 	= 'Amazon EU no vinculado';
$_['text_status_amazonus_saved'] 		= 'Amazon US guardado';
$_['text_status_amazonus_processing'] 	= 'Amazon US procesando';
$_['text_status_amazonus_active'] 		= 'Amazon US activo';
$_['text_status_amazonus_notlisted'] 	= 'Amazon estados unidos no esta incluido';
$_['text_status_amazonus_failed'] 		= 'Amazon US fallido';
$_['text_status_amazonus_linked'] 		= 'Amazon US vinculado';
$_['text_status_amazonus_notlinked'] 	= 'Amazon US no vinculado';
$_['text_processing']       			= 'Procesando';
$_['text_category_missing'] 			= 'Falta categoría';
$_['text_variations'] 					= 'Variaciones';
$_['text_variations_stock'] 			= 'stock';
$_['text_min']                      	= 'Min';
$_['text_max']                      	= 'Máx';
$_['text_option']                   	= 'Opción';

// Entry
$_['entry_title'] 						= 'Título';
$_['entry_model'] 						= 'Modelo';
$_['entry_manufacturer'] 				= 'Fabricante';
$_['entry_status'] 						= 'Estado';
$_['entry_status_marketplace'] 			= 'Situación de mercado';
$_['entry_stock_range'] 				= 'Rango de stock';
$_['entry_category'] 					= 'Categoría';
$_['entry_populated'] 					= 'Poblado';
$_['entry_sku'] 						= 'SKU';
$_['entry_description'] 				= 'Descripción';

// Button
$_['button_error_fix']              	= 'corregir errores';
$_['button_amazon_eu_bulk']         	= 'Amazon EU carga masiva';
$_['button_amazon_us_bulk']         	= 'Amazon US carga masiva';
$_['button_ebay_bulk']              	= 'Cargar eBay en masa';

// Error
$_['error_select_items']            	= 'Debes seleccionar al menos 1 artículo para listas en masa';