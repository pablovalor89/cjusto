<?php
// Headings
$_['heading_title']        		= 'Listados de Etsy';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_link_saved']           = 'El artículo se ha vinculado';
$_['text_activate']         	= 'Activar';
$_['text_deactivate']         	= 'Desactivar';
$_['text_add_link']         	= 'Añadir enlace';
$_['text_delete_link']         	= 'Borrar enlace';
$_['text_delete']         		= 'Borrar anuncio';
$_['text_status_stock']         = 'Stock no sincronizado';
$_['text_status_ok']         	= 'Aceptar';
$_['text_status_nolink']        = 'No vinculado';
$_['text_link_added']        	= 'Un producto ha sido vinculado al listado';
$_['text_link_deleted']        	= 'El producto ha sido desvinculado del listado';
$_['text_item_ended']        	= 'El articulo ha sido removido de Etsy';
$_['text_item_deactivated']     = 'El articulo ha sido desactivado de Etsy';
$_['text_item_activated']     	= 'El articulo ha sido activado en Etsy';
$_['text_confirm_end']          = '¿Está seguro que desea eliminar de la lista?';
$_['text_confirm_deactivate']   = '¿Está seguro que desea desactivar el listado?';
$_['text_confirm_activate']     = '¿Está seguro que desea activar el listado?';

// Columns
$_['column_listing_id']			= 'ID Etsy';
$_['column_title']				= 'Título';
$_['column_listing_qty']		= 'Cantidad de listado';
$_['column_store_qty']			= 'Cantidad de tienda';
$_['column_status']				= 'Mensaje de estado';
$_['column_link_status']		= 'Estado del enlace';
$_['column_action']				= 'Acción';

// Entry
$_['entry_limit']				= 'Límite de página';
$_['entry_status']				= 'Estado';
$_['entry_keywords']			= 'Palabras clave';
$_['entry_name']				= 'Nombre del producto';
$_['entry_etsy_id']				= 'ID de artículo de Etsy';

// Help
$_['help_keywords']				= 'Las palabras clave solo aplican a los listados activos';

// Error
$_['error_etsy']				= '¡Error! Respuesta de la API de Etsy: ';
$_['error_product_id']			= 'Identificación del producto requerida';
$_['error_etsy_id']				= 'ID de artículo de Etsy requerido';
