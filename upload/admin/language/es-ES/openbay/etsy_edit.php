<?php
// Headings
$_['heading_title']      		= 'Editar listado';
$_['text_openbay']          	= 'OpenBay Pro';
$_['text_etsy']             	= 'Etsy';

// Tabs

// Text
$_['text_option']      			= 'Seleccionar opción';
$_['text_listing_id']  			= 'Listado de ID';
$_['text_updated']  			= 'Su lista Etsy ha sido actualizada';
$_['text_edit']  				= 'Actualice su lista Etsy';

// Entry
$_['entry_title']      			= 'Título del producto';
$_['entry_description']     	= 'Descripción';
$_['entry_price']      			= 'Precio';
$_['entry_state']      			= 'Provincia/estado';

// Errors
$_['error_price_missing']  		= 'El precio está perdido o vacío';
$_['error_title_length']  		= 'Título es demasiado largo';
$_['error_title_missing']  		= 'Falta un título';
$_['error_desc_missing']  		= 'La descripción está perdida o vacía';
$_['error_price_missing']  		= 'El precio está perdido o vacío';
$_['error_state_missing']  		= 'El estado está perdido o vacío';