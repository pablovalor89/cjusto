<?php
// Headings
$_['heading_title']        		= 'Enlaces de Etsy';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_etsy']                 = 'Etsy';

// Text
$_['text_loading']              = 'Cargando artículos';
$_['text_new_link']             = 'Crear nuevo enlace';
$_['text_current_links']        = 'Enlaces actuales';
$_['text_link_saved']           = 'El artículo se ha vinculado';

// Columns
$_['column_product']			= 'Nombre del producto';
$_['column_item_id']			= 'Etsy ID';
$_['column_store_stock']		= 'Stock';
$_['column_etsy_stock']			= 'Inventario Etsy';
$_['column_status']				= 'Estado del enlace';
$_['column_action']				= 'Acción';

// Entry
$_['entry_name']				= 'Nombre del producto';
$_['entry_etsy_id']				= 'ID de artículo de Etsy';

// Error
$_['error_product']				= 'El producto no existe en su tienda';
$_['error_stock']				= 'No se puede vincular un elemento que no tiene stock';
$_['error_product_id']			= 'ID del producto requerido';
$_['error_etsy_id']				= 'ID de artículo de Etsy requerido';
$_['error_link_id']				= 'Enlace ID requerido';
$_['error_link_exists']			= 'Ya existe un enlace activo para este artículo';
$_['error_etsy']				= 'No se puede vincular el artículo, la respuesta de API de Etsy: ';
$_['error_status']				= 'Estado del filtro requerido';