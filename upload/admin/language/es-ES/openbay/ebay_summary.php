<?php
// Heading
$_['heading_title']             = 'Resumen de eBay';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_ebay']                 = 'eBay';

// Text
$_['text_use_desc']             = 'Esta es su pagina de resumen de su cuenta de eBay. Es una instantánea rápida de cualquier limite en su cuenta junto con su rendimiento de venta de DSR.';
$_['text_ebay_limit_head']      = 'Tu cuenta de eBay tiene límites de venta!';
$_['text_ebay_limit_t1']        = 'Usted puede vender';
$_['text_ebay_limit_t2']        = 'mas artículos (esto es la cantidad total de artículos, no listados individualmente) el valor de';
$_['text_ebay_limit_t3']        = 'Al intentar crear nuevos listados, fallará si se exceden las cantidades anteriores.';
$_['text_as_described']         = 'Articulo como se describe';
$_['text_communication']        = 'Comunicación';
$_['text_shippingtime']         = 'Tiempo del envío';
$_['text_shipping_charge']      = 'Gastos de envío';
$_['text_score']                = 'Puntuación';
$_['text_count']                = 'Cuenta';
$_['text_report_30']            = '30 días';
$_['text_report_52']            = '52 semanas';
$_['text_title_dsr']            = 'Informes DSR';
$_['text_failed']               = 'Error al cargar';

// Error
$_['error_validation']     		= 'Necesita registrarse para su token API y activar el módulo.';
$_['error_ajax_load']      		= 'Lo sentimos, error en la conexión al servidor';