<?php
// Heading
$_['heading_title']   			= 'Actualización masiva de pedidos';
$_['text_openbay']              = 'OpenBay Pro';
$_['text_confirm_title']        = 'Revisión de actualización de estado a granel';

// Button
$_['button_status']           	= 'Cambiar estado';
$_['button_update']           	= 'Actualizar';

// Column
$_['column_channel']        	= 'Orden de canal';
$_['column_additional']     	= 'Información adicional';
$_['column_comments']      		= 'Comentarios';
$_['column_notify']        		= 'Notificar';

// Text
$_['text_confirmed']            = '%s pedidos se han marcado %t';
$_['text_no_orders']            = 'Ninguna orden seleccionada para actualizar';
$_['text_confirm_change_text']  = 'Cambiar el estado de orden a';
$_['text_other']                = 'Otro';
$_['text_error_carrier_other']  = '¡Una orden le falta una entrada de "Otra compañía"!';
$_['text_web']                  = 'Web';
$_['text_ebay']                 = 'eBay';
$_['text_amazon']               = 'Amazon EU';
$_['text_amazonus']             = 'Amazon US';
$_['text_etsy']             	= 'Etsy';

// Entry
$_['entry_carrier']             = 'Transportista';
$_['entry_tracking_no']         = 'Seguimiento';
$_['entry_other']               = 'Otro';