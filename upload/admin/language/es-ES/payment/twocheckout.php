<?php
// Heading
$_['heading_title']					= '2Checkout';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Success: You have modified 2Checkout account details!';
$_['text_edit']                     = 'Editar 2Checkout';
$_['text_twocheckout']				= '<a href="https://www.2checkout.com/2co/affiliate?affiliate=1596408" target="_blank"><img src="view/image/payment/2checkout.png" alt="2Checkout" title="2Checkout" style="border: 1px solid #EEEEEE;" /></a>';

// Entry
$_['entry_account']					= 'ID de cuenta de 2Checkout';
$_['entry_secret']					= 'Palabra secreta';
$_['entry_display']					= 'Pago directo';
$_['entry_test']					= 'Modo de prueba';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_secret']					= 'The secret word to confirm transactions with (must be the same as defined on the merchant account configuration page).';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar pago 2Checkout!';
$_['error_account']					= '¡Nº. de cuenta obligatorio!';
$_['error_secret']					= 'Palabra secreta necesaria!';