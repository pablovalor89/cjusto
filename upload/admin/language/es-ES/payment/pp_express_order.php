<?php
// Text
$_['text_payment_info']				= 'Información de pago';
$_['text_capture_status']			= 'Capture status';
$_['text_amount_auth']				= 'Cantidad autorizada';
$_['text_amount_captured']			= 'Amount captured';
$_['text_amount_refunded']			= 'Cantidad reembolsada';
$_['text_capture_amount']			= 'Capture amount';
$_['text_complete_capture']			= 'Complete capture';
$_['text_transactions']				= 'Transacciones';
$_['text_complete']					= 'Completo';
$_['text_confirm_void']				= 'If you void you cannot capture any further funds';
$_['text_view']						= 'Ver';
$_['text_refund']					= 'Reembolso';
$_['text_resend']					= 'Reenviar';
$_['success_transaction_resent']	= 'Transaction was successfully resent';

// Column
$_['column_trans_id']				= 'ID de la Transacción';
$_['column_amount']					= 'Importe';
$_['column_type']					= 'Tipo de pago';
$_['column_status']					= 'Estado';
$_['column_pend_reason']			= 'Razón pendiente';
$_['column_date_added']				= 'Creado';
$_['column_action']					= 'Acción';

// Button
$_['button_void']					= 'Vacío';
$_['button_capture']				= 'Captura';

// Error
$_['error_capture_amt']				= 'Enter an amount to capture';
$_['error_timeout']					= 'Agotado el tiempo de solicitud';
$_['error_transaction_missing']		= 'No se encontró ninguna transacción';