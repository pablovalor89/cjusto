<?php
// Heading
$_['heading_title']					= 'NOCHEX';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Success: You have modified NOCHEX account details!';
$_['text_edit']                     = 'Editar NOCHEX';
$_['text_nochex']					= '<a href="https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798" target="_blank"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_seller']					= 'Vendedor / Cuenta personal';
$_['text_merchant']					= 'Cuenta de comerciante';

// Entry
$_['entry_email']					= 'E-Mail';
$_['entry_account']					= 'Tipo de cuenta';
$_['entry_merchant']				= 'ID comerciante';
$_['entry_template']				= 'Pass Template';
$_['entry_test']					= 'Test';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal!';
$_['error_email']					= '¡Correo electrónico requerido!';
$_['error_merchant']				= '¡ID comerciante requerida!';