<?php
// Heading
$_['heading_title']					= 'Reembolso de transacción';

// Text
$_['text_pp_express']				= 'PayPal Express Checkout';
$_['text_current_refunds']			= 'Refunds have already been done for this transaction. The max refund is';

// Entry
$_['entry_transaction_id']			= 'Id de la transacción';
$_['entry_full_refund']				= 'Reembolso completo';
$_['entry_amount']					= 'Importe';
$_['entry_message']					= 'Mensaje';

// Button
$_['button_refund']					= 'Emitir un reembolso';

// Error
$_['error_partial_amt']				= 'Debe introducir una cantidad de devolución parcial';
$_['error_data']					= 'Falta de petición de datos';