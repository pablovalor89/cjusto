<?php
// Heading
$_['heading_title']					= 'PayPal Payflow Pro';

// Text
$_['text_success']					= 'Éxito: ¡Has modificado los detalles de la cuenta PayPal Direct (UK)!';
$_['text_edit']                     = 'Editar PayPal Payflow Pro';
$_['text_pp_payflow']				= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Autorización';
$_['text_sale']						= 'Rebajas';

// Entry
$_['entry_vendor']					= 'Proveedor';
$_['entry_user']					= 'Usuario';
$_['entry_password']				= 'Contraseña';
$_['entry_partner']					= 'Socio';
$_['entry_test']					= 'Modo de prueba';
$_['entry_transaction']				= 'Método de transacción';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_vendor']					= 'Su ID de inicio de sesión de comerciante que creó cuando se registró en la cuenta Pro de Payments del sitio web';
$_['help_user']						= 'Si configura uno o más usuarios adicionales en la cuenta, este valor es la ID del usuario autorizado para procesar transacciones. Sin embargo, si no ha configurado usuarios adicionales en la cuenta, el USUARIO tiene el mismo valor que e VENDEDOR';
$_['help_password']					= 'La contraseña de 6 a 32 caracteres que se ha definido al registrar la cuenta';
$_['help_partner']					= 'La ID que le proporcionó el distribuidor autorizado de PayPal que lo registró para el SDK de Payflow. Si compró su cuenta directamente desde PayPal, use PayPal Pro en su lugar';
$_['help_test']						= '¿Utilizar el real o el servidor de prueba (sandbox) para procesar transacciones?';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal Website Payment Pro Checkout! (Reino Unido)';
$_['error_vendor']					= 'Vendor Required!';
$_['error_user']					= '¡Usuario requerido!';
$_['error_password']				= '¡Contraseña requerida!';
$_['error_partner']					= 'Partner Required!';