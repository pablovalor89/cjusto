<?php
// Heading
$_['heading_title']					= 'PayPoint';

// Text
$_['text_payment']					= 'Pago';
$_['text_success']					= 'Success: You have modified PayPoint account details!';
$_['text_edit']                     = 'Edit PayPoint';
$_['text_paypoint']					= '<a href="https://www.paypoint.net/partners/opencart" target="_blank"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']						= 'Producción';
$_['text_successful']				= 'Always Successful';
$_['text_fail']						= 'Always Fail';

// Entry
$_['entry_merchant']				= 'ID comerciante';
$_['entry_password']				= 'Contraseña remota';
$_['entry_test']					= 'Modo de prueba';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_password']					= 'Leave empty if you do not have "Digest Key Authentication" enabled on your account.';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal!';
$_['error_merchant']				= '¡ID comerciante requerida!';