<?php
// Heading
$_['heading_title']		 = 'Cheque / giro postal';

// Text
$_['text_payment']		 = 'Pagar';
$_['text_success']		 = 'Éxito: Ha modificado detalles de la cuenta de cheque / orden de pago!.';
$_['text_edit']          = 'Editar cheque/Orden de pago';

// Entry
$_['entry_payable']		 = 'Pagar a';
$_['entry_total']		 = 'Total';
$_['entry_order_status'] = 'Estado del Pedido';
$_['entry_geo_zone']	 = 'Zona geográfica';
$_['entry_status']		 = 'Estado';
$_['entry_sort_order']	 = 'Ordenar por';

// Help
$_['help_total']		 = 'El total a pagar de la orden a pagar debe ser alcanzado para que este método se active.';

// Error
$_['error_permission']   = 'ADVERTENCIA: No tienes permiso para modificar el cheque/Orden de pago!';
$_['error_payable']	     = 'Pagadero al requerirse!';