<?php
// Heading
$_['heading_title']					= 'PayPal Payflow Pro iFrame';
$_['heading_refund']				= 'Reembolso';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Éxito: ¡Ha modificado los detalles de las cuentas PayPal, Payflow y Pro iFrame!';
$_['text_edit']                     = 'EditarPaypal Payflow Pro iFrame';
$_['text_pp_payflow_iframe']		= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Autorización';
$_['text_sale']						= 'Rebajas';
$_['text_authorise']				= 'Autorizar';
$_['text_capture']					= 'Captura retrasada';
$_['text_void']						= 'Vacío';
$_['text_payment_info']				= 'Información de pago';
$_['text_complete']					= 'Completo';
$_['text_incomplete']				= 'Incompleto';
$_['text_transaction']				= 'Transacción';
$_['text_confirm_void']				= 'Si anula no podrá retener ningún monto futuro';
$_['text_refund']					= 'Reembolso';
$_['text_refund_issued']			= 'Reembolso fue emitido satisfactoriamente';
$_['text_redirect']					= 'Redirección';
$_['text_iframe']					= 'Iframe';
$_['help_checkout_method']			= 'Por favor use el método redirigir si no tiene SSL instalado o si no tiene la opción de pago con paypal deshabilitada en su pagina de pago alojada.';

// Column
$_['column_transaction_id']			= 'ID de la Transacción';
$_['column_transaction_type']		= 'Tipo de transacción';
$_['column_amount']					= 'Importe';
$_['column_time']					= 'Hora';
$_['column_actions']				= 'Acciones';

// Tab
$_['tab_settings']					= 'Configuración';
$_['tab_order_status']				= 'Estado del pedido';
$_['tab_checkout_customisation']	= 'Personalización de checkout';

// Entry
$_['entry_vendor']					= 'Vendedor';
$_['entry_user']					= 'Usuario';
$_['entry_password']				= 'Contraseña';
$_['entry_partner']					= 'Compañero';
$_['entry_test']					= 'Modo de prueba';
$_['entry_transaction']				= 'Método de transacción';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';
$_['entry_transaction_id']			= 'ID de la Transacción';
$_['entry_full_refund']				= 'Reembolso completo';
$_['entry_amount']					= 'Importe';
$_['entry_message']					= 'Mensaje';
$_['entry_ipn_url']					= 'IPN URL';
$_['entry_checkout_method']			= 'Método de checkout';
$_['entry_debug']					= 'Modo de depuración';
$_['entry_transaction_reference']	= 'Referencia de la transacción';
$_['entry_transaction_amount']		= 'Cantidad de la transacción';
$_['entry_refund_amount']			= 'Cantidad de Rembolso';
$_['entry_capture_status']			= 'Estatus de captura';
$_['entry_void']					= 'Vacío';
$_['entry_capture']					= 'Captura';
$_['entry_transactions']			= 'Transacciones';
$_['entry_complete_capture']		= 'Captura completa';
$_['entry_canceled_reversal_status'] = 'Estado de revocación cancelado:';
$_['entry_completed_status']		= 'Estado completado:';
$_['entry_denied_status']			= 'Estado denegado:';
$_['entry_expired_status']			= 'Estatus expirado:';
$_['entry_failed_status']			= 'Estatus fallido:';
$_['entry_pending_status']			= 'Estado pendiente:';
$_['entry_processed_status']		= 'Estatus Procesado:';
$_['entry_refunded_status']			= 'Estatus reembolsado:';
$_['entry_reversed_status']			= 'Estatus Revertido:';
$_['entry_voided_status']			= 'Estatus Anulado:';
$_['entry_cancel_url']				= 'URL de cancelación:';
$_['entry_error_url']				= 'Error de URL:';
$_['entry_return_url']				= 'URL devuelta:';
$_['entry_post_url']				= 'URL POST silencioso:';

// Help
$_['help_vendor']					= 'Su ID de inicio de sesión de comerciante que creó cuando se registró en la cuenta Pro de Payments del sitio web';
$_['help_user']						= 'Si configura uno o más usuarios adicionales en la cuenta, este valor es la ID del usuario autorizado para procesar transacciones. Sin embargo, si no ha configurado usuarios adicionales en la cuenta, el USUARIO tiene el mismo valor que e VENDEDOR';
$_['help_password']					= 'La contraseña de 6 a 32 caracteres que se ha definido al registrar la cuenta';
$_['help_partner']					= 'La ID que le proporcionó el distribuidor autorizado de PayPal que lo registró para el SDK de Payflow. Si compró su cuenta directamente desde PayPal, use PayPal Pro en su lugar';
$_['help_test']						= '¿Utilizar el real o el servidor de prueba (sandbox) para procesar transacciones?';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active';
$_['help_debug']					= 'Información adicional Registro';

// Button
$_['button_refund']					= 'Reembolso';
$_['button_void']					= 'Vacío';
$_['button_capture']				= 'Captura';

// Error
$_['error_permission']				= 'Advertencia: Usted no tiene permitido modificar el pago PayPal Website Payment Pro ¡Frame (UK)!';
$_['error_vendor']					= '¡Proveedor Requerido!';
$_['error_user']					= '¡Usuario requerido!';
$_['error_password']				= '¡Contraseña requerida!';
$_['error_partner']					= '¡Socio Requerido!';
$_['error_missing_data']			= 'Datos desaparecidos';
$_['error_missing_order']			= 'No pudimos encontrar la orden';
$_['error_general']					= 'Se ha producido un error';
$_['error_capture_amt']				= 'Ingrese una cantidad para retencion';