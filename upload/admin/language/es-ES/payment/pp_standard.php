<?php
// Heading
$_['heading_title']					= 'PayPal Payments Standard';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Correcto: Ha modificado los detalles de la cuenta de PayPal!';
$_['text_edit']                     = 'Editar PayPal Payments Standard';
$_['text_pp_standard']				= '<a target="_BLANK" href="https://www.paypal.com/uk/mrb/pal=V4T754QB63XXL"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro iFrame" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization']			= 'Autorización';
$_['text_sale']						= 'Rebajas';

// Entry
$_['entry_email']					= 'E-Mail';
$_['entry_test']					= 'Modo Sandbox';
$_['entry_transaction']				= 'Método de transacción';
$_['entry_debug']					= 'Modo de depuración';
$_['entry_total']					= 'Total ';
$_['entry_canceled_reversal_status'] = 'Estado de revocación cancelado';
$_['entry_completed_status']		= 'Estado completado';
$_['entry_denied_status']			= 'Estado denegado';
$_['entry_expired_status']			= 'Estado caducado';
$_['entry_failed_status']			= 'Estado fallido';
$_['entry_pending_status']			= 'Estado pendiente';
$_['entry_processed_status']		= 'Estado procesado';
$_['entry_refunded_status']			= 'Estado reembolsado';
$_['entry_reversed_status']			= 'Estado invertida';
$_['entry_voided_status']			= 'Estado anulado';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Tab
$_['tab_general']					= 'General';
$_['tab_status']					= 'Estado del pedido';

// Help
$_['help_test']						= '¿Utilice el real o el de servidor de prueba (sandbox) para procesar transacciones?';
$_['help_debug']					= 'Información adicional de los registros en el registro del sistema';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active';

// Error
$_['error_permission']				= 'ADVERTENCIA: No tienes permiso para modificar el pago PayPal!';
$_['error_email']					= '¡Correo electrónico requerido!';