<?php
// Heading
$_['heading_title']					= 'Pagos perpetuos';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Éxito: Ha modificado los detalles de la cuenta de pagos perpetuo!';
$_['text_edit']                     = 'Editar pagos perpetuos';

// Entry
$_['entry_auth_id']					= 'ID de autorización';
$_['entry_auth_pass']				= 'Contraseña de autorización';
$_['entry_test']					= 'Modo de prueba';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_test']						= '¿Utilice este módulo en modo de prueba (sí) o producción (NO)?';
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: No tienes permiso para modificar pagos perpetuos!';
$_['error_auth_id']					= 'Se requiere el ID de autorización!';
$_['error_auth_pass']				= 'Se requiere password de autorización!';