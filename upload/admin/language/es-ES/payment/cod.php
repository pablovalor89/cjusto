<?php
// Heading
$_['heading_title']					= 'Contrareembolso';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= 'Correcto: Ha modificado el  módulo de pago contrareembolso!';
$_['text_edit']                     = 'Modificar contra-reembolso';

// Entry
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: No tienes permiso para modificar el pago contrareembolso!';