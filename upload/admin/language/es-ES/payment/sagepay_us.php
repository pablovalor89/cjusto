<?php
// Heading
$_['heading_title']					= 'Sage Payment Solutions (US)';

// Text
$_['text_payment']					= 'Pagar';
$_['text_success']					= '! Éxito: Ha modificado los detalles de la cuenta de SagePay!';
$_['text_edit']                     = 'Editar Sage Payment Solutions (US)';

// Entry
$_['entry_merchant_id']				= 'ID comerciante';
$_['entry_merchant_key']			= 'Clave de comerciante';
$_['entry_total']					= 'Total ';
$_['entry_order_status']			= 'Estado del pedido';
$_['entry_geo_zone']				= 'Zona geográfica';
$_['entry_status']					= 'Estado';
$_['entry_sort_order']				= 'Ordenar por';

// Help
$_['help_total']					= 'El total a pagar que el pedido debe alcanzar para que este método se active.';

// Error
$_['error_permission']				= 'ADVERTENCIA: ¡No tienes permiso para modificar el pago PayPal!';
$_['error_merchant_id']				= '¡ID comerciante requerida!';
$_['error_merchant_key']			= 'Llave mercantil requerida!';