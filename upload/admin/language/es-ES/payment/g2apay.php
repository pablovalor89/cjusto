<?php
// Heading
$_['heading_title']				 		 = 'G2Apay';

// Text
$_['text_payment']				 		 = 'Pago';
$_['text_success']				 		 = 'Éxito: Ha modificado detalles de G2APay.';
$_['text_edit']					 		 = 'Editar G2APay';
$_['text_g2apay']				 		 = '<img src="view/image/payment/g2apay.png" alt="G2APay" title="G2APay" style="border: 1px solid #EEEEEE;" />';

$_['entry_username']			 		 = 'Usuario';
$_['entry_secret']				 		 = 'Secreto';
$_['entry_api_hash']		     		 = 'API picado';
$_['entry_environment']			 		 = 'Ambiente';
$_['entry_secret_token']		 		 = 'Token secreto';
$_['entry_ipn_url']				 		 = 'IPN URL:';
$_['entry_total']				 		 = 'Total ';

$_['entry_geo_zone']			 		 = 'Zona geográfica';
$_['entry_status']				 		 = 'Estado';
$_['entry_sort_order']			 		 = 'Ordenar por';
$_['entry_debug']				 		 = 'Depurar registro';

$_['entry_order_status']				 = 'Estado del pedido';
$_['entry_complete_status']				 = 'Estatus Completo:';
$_['entry_rejected_status']				 = 'Estatus rechazado:';
$_['entry_cancelled_status']			 = 'Estatus Cancelado:';
$_['entry_refunded_status']				 = 'Estatus reembolsado:';
$_['entry_partially_refunded_status']	 = 'Estatus Parcialmente Reembolsado:';

// Help
$_['help_username']						 = 'La dirección de email usada para su cuenta';
$_['help_total']				 		 = 'El total a pagar que el pedido debe alcanzar para que este método se active.';
$_['help_debug']				 		 = 'Habilitando la depuración escribirá datos sensibles en el registro de archivos. Debería siempre inhabilitar a menos que se le indique lo contrario';

// Tab
$_['tab_settings']				 		 = 'Configuración';
$_['tab_order_status']				 	 = 'Estado del pedido';

// Error
$_['error_permission']			 		 = 'Advertencia: No tiene permiso de modificar G2APay!';
$_['error_email']				 		 = '¡Correo electrónico requerido!';
$_['error_secret']				 		 = '¡Secreto Requerido!';
$_['error_api_hash']			 		 = '¡Contraseña API Requerida!';
$_['entry_status']				 		 = 'Estado';
$_['entry_order_status']		 		 = 'Estado del pedido';

// Order page - payment tab
$_['text_payment_info']			 		 = 'Información de pago';
$_['text_refund_status']		 		 = 'Reembolso de pago';
$_['text_order_ref']			 		 = 'Ref. de pedido';
$_['text_order_total']			 		 = 'Total autorizado';
$_['text_total_released']		 		 = 'Total liberado';
$_['text_transactions']			 		 = 'Transacciones';
$_['text_column_amount']		 		 = 'Importe';
$_['text_column_type']			 		 = 'Tipo';
$_['text_column_date_added']	 		 = 'Agregado';
$_['text_refund_ok']			 		 = 'El reembolso fue solicitado con éxito';
$_['text_refund_ok_order']		 		 = 'El reembolso fue solicitado con éxito, cantidad totalmente reembolsada';

//Button
$_['btn_refund']				 		 = 'reembolso';

$_['g2apay_environment_live']	 		 = 'En vivo';
$_['g2apay_environment_test']	 		 = 'Test';