<?php
// Heading
$_['heading_title']         = 'Clientes';

// Text
$_['text_success']          = '¡Éxito: Ha modificado clientes!';
$_['text_list']             = 'Lista de clientes';
$_['text_add']              = 'Añadir cliente';
$_['text_edit']             = 'Editar cliente';
$_['text_default']          = 'Predefinido';
$_['text_balance']          = 'Saldo';
$_['text_add_ban_ip']       = 'Añadir Bloqueo de IP';
$_['text_remove_ban_ip']    = 'Eliminar bloqueo de IP';

// Column
$_['column_name']           = 'Nombre del cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de Clientes';
$_['column_status']         = 'Estado';
$_['column_date_added']     = 'Agregar Fecha';
$_['column_comment']        = 'Comentario';
$_['column_description']    = 'Descripción';
$_['column_amount']         = 'Importe';
$_['column_points']         = 'Puntos';
$_['column_ip']             = 'IP';
$_['column_total']          = 'Cuentas Totales';
$_['column_action']         = 'Acción';

// Entry
$_['entry_customer_group']  = 'Grupo de Clientes';
$_['entry_firstname']       = 'Nombre';
$_['entry_lastname']        = 'Apellidos';
$_['entry_email']           = 'E-Mail';
$_['entry_telephone']       = 'Teléfono';
$_['entry_fax']             = 'Fax';
$_['entry_newsletter']      = 'Boletín de noticias';
$_['entry_status']          = 'Estado';
$_['entry_approved']        = 'Aprobado';
$_['entry_safe']            = 'Seguro';
$_['entry_password']        = 'Contraseña';
$_['entry_confirm']         = 'Confirmar';
$_['entry_company']         = 'Empresa';
$_['entry_address_1']       = 'Dirección 1';
$_['entry_address_2']       = 'Dirección 2';
$_['entry_city']            = 'Ciudad';
$_['entry_postcode']        = 'Código postal';
$_['entry_country']         = 'País';
$_['entry_zone']            = 'Región / Provincia';
$_['entry_default']         = 'Dirección Principal';
$_['entry_comment']         = 'Comentario';
$_['entry_description']     = 'Descripción';
$_['entry_amount']          = 'Importe';
$_['entry_points']          = 'Puntos';
$_['entry_name']            = 'Nombre del cliente';
$_['entry_ip']              = 'IP';
$_['entry_date_added']      = 'Agregar Fecha';

// Help
$_['help_safe']             = 'Seleccione verdadero para prevenir que este cliente sea bloqueado por el sistema anti-fraude';
$_['help_points']           = 'Utilice el signo menos para quitar puntos';

// Error
$_['error_warning']         = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']      = 'ADVERTENCIA: No tienes permiso para modificar los usuarios!';
$_['error_exists']          = 'ADVERTENCIA: Dirección de correo electrónico ya está registrada!';
$_['error_firstname']       = 'El nombre debe tener entre 1 y 21 caracteres!';
$_['error_lastname']        = 'El apellido debe tener entre 1 y 21 caracteres!';
$_['error_email']           = 'La dirección de correo electrónico no parece ser válida!';
$_['error_telephone']       = '¡El teléfono debe tener entre 3 y 32 caracteres!';
$_['error_password']        = 'La Contraseña debe poseer entre 4 y 20 caracteres!';
$_['error_confirm']         = 'La contraseña y su confirmación no coinciden!';
$_['error_address_1']       = 'La dirección debe ser entre 3 y 128 caracteres!';
$_['error_city']            = 'La ciudad debe ser entre 2 y 128 caracteres!';
$_['error_postcode']        = 'El código postal debe ser entre 2 y 10 caracteres para este país!';
$_['error_country']         = 'Por favor seleccione un Pais!';
$_['error_zone']            = 'Por favor, seleccione una región / estado!';
$_['error_custom_field']    = '%s requerido!';
$_['error_comment']         = 'Debe introducir un comentario!';
