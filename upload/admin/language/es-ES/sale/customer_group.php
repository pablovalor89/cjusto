<?php
// Heading
$_['heading_title']     = 'Grupos de clientes';

// Text
$_['text_success']      = '¡Éxito: Ha modificado los grupos de clientes!';
$_['text_list']         = 'Lista de grupo de clientes';
$_['text_add']          = 'Añadir grupo de clientes';
$_['text_edit']         = 'Editar grupo del cliente';

// Column
$_['column_name']       = 'Nombre del grupo de clientes';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre del grupo de clientes';
$_['entry_description'] = 'Descripción';
$_['entry_approval']    = 'Aprobar nuevos clientes';
$_['entry_sort_order']  = 'Ordenar por';

// Help
$_['help_approval']     = 'Los clientes deben ser aprobados por un administrador antes de poder iniciar sesión.';

// Error
$_['error_permission']   = '¡Advertencia: No tiene permisos para modificar los grupos de clientes!';
$_['error_name']         = '¡El Nombre del grupo del cliente debe tener entre 3 y 32 caracteres!';
$_['error_default']      = '¡Advertencia: Este grupo de clientes no se puede eliminar porque está asignado como el grupo de clientes predefinido!';
$_['error_store']        = '¡Advertencia: Este grupo de clientes no se puede eliminar porque está asignado en %s tiendas!';
$_['error_customer']     = '¡Advertencia: Este grupo de clientes no se puede eliminar porque está asignado en %s clientes!';