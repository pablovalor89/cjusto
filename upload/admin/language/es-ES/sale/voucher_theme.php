<?php
// Heading
$_['heading_title']     = 'Plantillas de Bonos';

// Text
$_['text_success']      = 'Éxito: Has modificado los temas de vouchers!';
$_['text_list']         = 'Lista de temas de vouchers';
$_['text_add']          = 'Agrega un tema de vouchers';
$_['text_edit']         = 'Edita un tema de vouchers';

// Column
$_['column_name']       = 'Nombre del tema de vouchers';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre del tema de vouchers';
$_['entry_description'] = 'Descripción del tema de Vouchers';
$_['entry_image']       = 'Imagen';

// Error
$_['error_permission']  = 'Cuidado: No tienes permisos para modificar los temas de vouchers!';
$_['error_name']        = 'Los nombres de los temas de vouchers deben tener entre 3 y 32 caracteres!';
$_['error_image']       = 'Imagen requerida!';
$_['error_voucher']     = 'Cuidado: Éste tema de voucher no puede ser borrado, ya que está actualmente asignado a %s vouchers!';