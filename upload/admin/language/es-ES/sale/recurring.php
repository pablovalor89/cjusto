<?php
// Heading
$_['heading_title']                        = 'Perfiles Recurrentes';

// Text
$_['text_success']                         = '¡Has modificado correctamente los perfiles recurrentes!';
$_['text_list']                            = 'Lista de Perfiles Recurrentes';
$_['text_add']                             = 'Agregar Perfil Recurrente';
$_['text_edit']                            = 'Editar Perfil Recurrente';
$_['text_payment_profiles']                = 'Perfiles Recurrentes';
$_['text_status_active']                   = 'Activo';
$_['text_status_inactive']                 = 'Inactivo';
$_['text_status_cancelled']                = 'Cancelado';
$_['text_status_suspended']                = 'Suspendido';
$_['text_status_expired']                  = 'Expirado';
$_['text_status_pending']                  = 'Pendiente';
$_['text_transactions']                    = 'Transacciones';
$_['text_cancel_confirm']                  = '¡La cancelación del perfil no se puede deshacer! ¿Está seguro de que desea hacer esto?';
$_['text_transaction_date_added']          = 'Fecha creación';
$_['text_transaction_payment'] 			   = 'Pagar';
$_['text_transaction_outstanding_payment'] = 'Pago restante';
$_['text_transaction_skipped']             = 'Pago omitido';
$_['text_transaction_failed']              = 'Pago fallido';
$_['text_transaction_cancelled']           = 'Cancelado';
$_['text_transaction_suspended']           = 'Suspendido';
$_['text_transaction_suspended_failed']    = 'Suspendido por fallo en el pago';
$_['text_transaction_outstanding_failed']  = 'Pago restante fallido';
$_['text_transaction_expired']             = 'Caducó';

// Entry
$_['entry_cancel_payment']                 = 'Cancelar pago';
$_['entry_order_recurring']                = 'ID';
$_['entry_order_id']                       = 'ID del Pedido';
$_['entry_reference']                      = 'Referencia de pago';
$_['entry_customer']                       = 'Cliente';
$_['entry_date_added']                     = 'Agregar Fecha';
$_['entry_status']                         = 'Estado';
$_['entry_type']                           = 'Tipo';
$_['entry_action']                         = 'Acción';
$_['entry_email']                          = 'Correo electrónico';
$_['entry_description']                    = 'Descripción del perfil recurrente';
$_['entry_product']                        = 'Producto';
$_['entry_quantity']                       = 'Cantidad';
$_['entry_amount']                         = 'Importe';
$_['entry_recurring']                      = 'Perfil Recurrente';
$_['entry_payment_method']                 = 'Modalidad de Pago';

// Error / Success
$_['error_not_cancelled']                  = 'Error: %s';
$_['error_not_found']                      = 'No se pudo cancelar el perfil recurrente';
$_['text_cancelled']                    = 'El pago recurrente ha sido cancelado';
