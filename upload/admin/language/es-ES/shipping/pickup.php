<?php
// Heading
$_['heading_title']    = 'Recoger en tienda';

// Text
$_['text_shipping']    = 'Envío';
$_['text_success']     = 'Éxito: ¡Se ha modificado la recogida en tienda!';
$_['text_edit']        = 'Editar transporte de envío del almacén';

// Entry
$_['entry_geo_zone']   = 'Zona geográfica';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: ¡No tienes permiso para modificar la recogida en tienda!';