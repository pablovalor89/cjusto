<?php
// Heading
$_['heading_title']      = 'Australia Post';

// Text
$_['text_shipping']      = 'Envío';
$_['text_success']       = 'Éxito: ¡Has modificado envío de Australia Post!';
$_['text_edit']          = 'Editar envío de Australia Post';

// Entry
$_['entry_postcode']     = 'Código postal';
$_['entry_express']      = 'Gastos de envío Express';
$_['entry_standard']     = 'Gastos de envío estándar';
$_['entry_display_time'] = 'Mostrar tiempo de entrega';
$_['entry_weight_class'] = 'Medida de Peso';
$_['entry_tax_class']    = 'Tipo de Impuesto';
$_['entry_geo_zone']     = 'Zona geográfica';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Ordenar por';

// Help
$_['help_display_time']  = '¿Quieres mostrar el tiempo de envío? (ej. envío dentro de 3 a 5 días)';
$_['help_weight_class']  = 'Establecer en gramos.';

// Error
$_['error_permission']   = 'ADVERTENCIA: ¡No tienes permiso para modificar el envío Australia Post!';
$_['error_postcode']     = 'El código postal debe ser de 4 dígitos!';