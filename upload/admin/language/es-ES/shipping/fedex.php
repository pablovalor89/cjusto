<?php
// Heading
$_['heading_title']                            = 'Fedex';

// Text
$_['text_shipping']                            = 'Envío';
$_['text_success']                             = 'Éxito: ¡Has modificado envío Fedex!';
$_['text_edit']                                = 'Editar envío Fedex';
$_['text_europe_first_international_priority'] = 'Europe First International Priority';
$_['text_fedex_1_day_freight']                 = 'Fedex 1 Day Freight';
$_['text_fedex_2_day']                         = 'Fedex 2 Day';
$_['text_fedex_2_day_am']                      = 'Fedex 2 Day AM';
$_['text_fedex_2_day_freight']                 = 'Fedex 2 Day Freight';
$_['text_fedex_3_day_freight']                 = 'Fedex 3 Day Freight';
$_['text_fedex_express_saver']                 = 'Fedex Express Saver';
$_['text_fedex_first_freight']                 = 'Fedex First Fright';
$_['text_fedex_freight_economy']               = 'Fedex Fright Economy';
$_['text_fedex_freight_priority']              = 'Fedex Fright Priority';
$_['text_fedex_ground']                        = 'Fedex Ground';
$_['text_first_overnight']                     = 'First Overnight';
$_['text_ground_home_delivery']                = 'Ground Home Delivery';
$_['text_international_economy']               = 'Economíco internacional';
$_['text_international_economy_freight']       = 'International Economy Freight';
$_['text_international_first']                 = 'International First';
$_['text_international_priority']              = 'International Priority';
$_['text_international_priority_freight']      = 'International Priority Freight';
$_['text_priority_overnight']                  = 'Priority Overnight';
$_['text_smart_post']                          = 'Post inteligente';
$_['text_standard_overnight']                  = 'Standard Overnight';
$_['text_regular_pickup']                      = 'Recogida ordinaria';
$_['text_request_courier']                     = 'Request Courier';
$_['text_drop_box']                            = 'Drop Box';
$_['text_business_service_center']             = 'Centro de servicos de negocios';
$_['text_station']                             = 'Estación';
$_['text_fedex_envelope']                      = 'FedEx Envelope';
$_['text_fedex_pak']                           = 'FedEx Pak';
$_['text_fedex_box']                           = 'Caja de FedEx';
$_['text_fedex_tube']                          = 'FedEx Tube';
$_['text_fedex_10kg_box']                      = 'Caja de 10 Kg. de FedEx';
$_['text_fedex_25kg_box']                      = 'FedEx 25kg Box';
$_['text_your_packaging']                      = 'Su embalaje';
$_['text_list_rate']                           = 'List Rate';
$_['text_account_rate']                        = 'Account Rate';

// Entry
$_['entry_key']                                = 'Key';
$_['entry_password']                           = 'Contraseña';
$_['entry_account']                            = 'Número de cuenta';
$_['entry_meter']                              = 'Meter Number';
$_['entry_postcode']                           = 'Código postal';
$_['entry_test']                               = 'Modo de prueba';
$_['entry_service']                            = 'Services';
$_['entry_dimension']                          = 'Box Dimensions (L x W x H)';
$_['entry_length_class']                       = 'Tipo de Longitud';
$_['entry_length']                             = 'Longitud';
$_['entry_width']                              = 'Ancho';
$_['entry_height']                             = 'Alto';
$_['entry_dropoff_type']                       = 'Drop Off Type';
$_['entry_packaging_type']                     = 'Packaging Type';
$_['entry_rate_type']                          = 'Rate Type';
$_['entry_display_time']                       = 'Mostrar tiempo de entrega';
$_['entry_display_weight']                     = 'Display Delivery Weight';
$_['entry_weight_class']                       = 'Medida de Peso';
$_['entry_tax_class']                          = 'Tipo de Impuesto';
$_['entry_geo_zone']                           = 'Zona geográfica';
$_['entry_status']                             = 'Estado';
$_['entry_sort_order']                         = 'Ordenar por';

// Help
$_['help_length_class']                        = 'Set to inches or centimeters.';
$_['help_display_time']                        = '¿Quieres mostrar el tiempo de envío? (ej. envío dentro de 3 a 5 días)';
$_['help_display_weight']                      = 'Do you want to display the shipping weight? (e.g. Delivery Weight : 2.7674 kg)';
$_['help_weight_class']                        = 'Set to kilograms or pounds.';

// Error
$_['error_permission']                         = 'ADVERTENCIA: ¡No tienes permiso para modificar el envío Fedex!';
$_['error_key']                                = '¡Clave necesaria!';
$_['error_password']                           = '¡Contraseña requerida!';
$_['error_account']                            = '¡Cuenta necesaria!';
$_['error_meter']                              = 'Meter required!';
$_['error_postcode']                           = '¡Código postal necesario!';
$_['error_dimension']                          = '¡Anchura y altura requeridas!';