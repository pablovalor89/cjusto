<?php
// Heading
$_['heading_title']           = 'Parcelforce 48';

// Text
$_['text_shipping']           = 'Envío';
$_['text_success']            = 'Success: You have modified Parcelforce 48 shipping!';
$_['text_edit']               = 'Edit Parcelforce 48 Shipping';

// Entry
$_['entry_rate']              = 'Parcelforce 48 Rates';
$_['entry_insurance']         = 'Parcelforce48 Compensation Rates';
$_['entry_display_weight']    = 'Mostrar Peso de Entrega';
$_['entry_display_insurance'] = 'Desplegar seguro';
$_['entry_display_time']      = 'Mostrar tiempo de entrega';
$_['entry_tax_class']         = 'Tipo de Impuesto';
$_['entry_geo_zone']          = 'Zona geográfica';
$_['entry_status']            = 'Estado';
$_['entry_sort_order']        = 'Ordenar por';

// Help
$_['help_rate']              = 'Ingresar valores decimales de hasta 5,2. Ejemplo (12345.67):. 1:1,.25:1.27 - peso menor o igual a 0.1 Kg costaría 1,00&euro;, peso inferior o igual a 0,25 g, pero más de 0,1 Kg tendrá un costo de 1,27. No introduzcas KG o símbolos.';
$_['help_insurance']         = 'Ingrese valores arriba de 5,2 lugares decimales. (12345.67) Ejemplo: 34:0,100:1,250:2.25 - Seguro para cubrir valores del carrito hasta 34 costará 0.00 extra, aquellos valores de más de 100 y hasta 250 costará 2.25 extra. No ingrese símbolos de moneda.';
$_['help_display_weight']    = '¿Quieres mostrar el peso del envío? (p.ej. peso de entrega: 2,7674 kgs)';
$_['help_display_insurance'] = '¿Quieres mostrar el seguro de envío? (e.g. asegurado hasta &pound;500)';
$_['help_display_time']      = '¿Quieres mostrar el tiempo de envío? (ej. envío dentro de 3 a 5 días)';

// Error
$_['error_permission']        = 'ADVERTENCIA: ¡No tienes permiso para modificar el envío de Parcelforce 48!';