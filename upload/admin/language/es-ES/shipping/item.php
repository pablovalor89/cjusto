<?php
// Heading
$_['heading_title']     = 'Por artículo';

// Text
$_['text_shipping']    = 'Envío';
$_['text_success']     = 'Éxito: Ha modificado tarifas de envío por artículo!';
$_['text_edit']        = 'Editar envío por artículo';

// Entry
$_['entry_cost']       = 'Coste';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_geo_zone']   = 'Zona geográfica';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tiene permiso para modificar tarifas de envío por artículo !';