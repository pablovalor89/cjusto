<?php
// Heading
$_['heading_title']    = 'Envío basado en el peso';

// Text
$_['text_shipping']    = 'Envío';
$_['text_success']     = 'Éxito: Ha modificado envío basado en peso!';
$_['text_edit']        = 'Editar envío basado en peso';

// Entry
$_['entry_rate']       = 'Tasas';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_geo_zone']   = 'Zona geográfica';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de aparición';

// Help
$_['help_rate']        = 'Ejemplo: 5:10. 00, 7:12.00 peso: costo, peso: costo, etc...';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar envío basado en peso!';