<?php
// Heading
$_['heading_title']     = 'Grupos de Usuarios';

// Text
$_['text_success']      = 'Éxito: ¡Ha modificado los grupos de clientes!';
$_['text_list']         = 'Grupos de Usuario';
$_['text_add']          = 'Añadir Grupo de Usuarios';
$_['text_edit']         = 'Editar grupo de usuarios';

// Column
$_['column_name']       = 'Nombre del grupo de usuarios';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre del grupo de usuarios';
$_['entry_access']      = 'Permisos de Acceso';
$_['entry_modify']      = 'Modificar permisos';

// Error
$_['error_permission']  = '¡Advertencia: No tiene permisos para modificar los grupos de clientes!';
$_['error_name']        = '¡El nombre del Grupo de Filtros debe tener entre 1 y 64 carácteres!';
$_['error_user']        = '¡Advertencia: Este grupo de clientes no se puede eliminar porque está asignado en %s clientes!';