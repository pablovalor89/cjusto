<?php
// Heading
$_['heading_title']        = 'API\'s';

// Text
$_['text_success']         = 'Éxito: Ha modificado la API.';
$_['text_list']            = 'Lista de API';
$_['text_add']             = 'Añadir API';
$_['text_edit']            = 'Editar API';

// Column
$_['column_username']      = 'Usuario';
$_['column_status']        = 'Estado';
$_['column_date_added']    = 'Agregar Fecha';
$_['column_date_modified'] = 'Fecha Modificación';
$_['column_action']        = 'Acción';

// Entry
$_['entry_username']       = 'Usuario';
$_['entry_password']       = 'Contraseña';
$_['entry_status']         = 'Estado';

// Error
$_['error_permission']     = '¡Advertencia: No tienes permiso para modificar API\'s!';
$_['error_username']       = '¡Nombre de usuario debe tener entre 3 y 20 caracteres!';
$_['error_password']       = 'La contraseña de la API debe ser entre 3 y 256 caracteres!';