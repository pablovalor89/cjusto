<?php
// Heading
$_['heading_title']     = 'Últimos pedidos';

// Column
$_['column_order_id']   = 'ID del Pedido';
$_['column_customer']   = 'Cliente';
$_['column_status']     = 'Estado';
$_['column_total']      = 'Total ';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';