<?php
// Heading
$_['heading_title']                = 'Actividad Reciente';

// Text
$_['text_customer_address_add']    = '<a href="customer_id=%d"> %s</a> agregó una nueva dirección.';
$_['text_customer_address_edit']   = '<a href="customer_id=%d"> %s</a> ha actualizado su dirección.';
$_['text_customer_address_delete'] = '<a href="customer_id=%d"> %s</a> ha borrado una de sus direcciones.';
$_['text_customer_edit']           = '<a href="customer_id=%d"> %s</a> ha actualizado la información de su cuenta.';
$_['text_customer_forgotten']      = '<a href="customer_id=%d"> %s</a> ha solicitado una nueva contraseña.';
$_['text_customer_login']          = '<a href="customer_id=%d"> %s</a> se ha conectado.';
$_['text_customer_password']       = '<a href="customer_id=%d"> %s</a> ha actualizado su contraseña.';
$_['text_customer_register']       = '<a href="customer_id=%d"> %s</a> ha registrado una nueva cuenta.';
$_['text_customer_return_account'] = '<a href="customer_id=%d"> %s</a> presentó una devolución de producto.';
$_['text_customer_return_guest']   = '%s presentó una devolución de producto.';
$_['text_customer_order_account']  = '<a href="customer_id=%d"> %s</a> date_added el pedido <a href="order_id=%d"></a>.';
$_['text_customer_order_guest']    = '%s creó un el pedido <a href="order_id=%d"></a>.';
$_['text_affiliate_edit']          = '<a href="affiliate_id=%d"> %s</a> ha actualizado la información de su cuenta.';
$_['text_affiliate_forgotten']     = '<a href="affiliate_id=%d"> %s</a> solicitó una nueva contraseña.';
$_['text_affiliate_login']         = '<a href="affiliate_id=%d"> %s</a> se ha conectado.';
$_['text_affiliate_password']      = '<a href="affiliate_id=%d"> %s</a> ha actualizado la contraseña de su cuenta.';
$_['text_affiliate_payment']       = '<a href="affiliate_id=%d"> %s</a> ha actualizado la información de su cuenta.';
$_['text_affiliate_register']      = '<a href="affiliate_id=%d"> %s</a> ha registrado una nueva cuenta.';