<?php
// Heading
$_['heading_title']         = 'Reporte de Puntos de Recompensa del cliente';

// Text
$_['text_list']             = 'Listado de Puntos de Recompensa del cliente';

// Column
$_['column_customer']       = 'Nombre del cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de Clientes';
$_['column_status']         = 'Estado';
$_['column_points']         = 'Puntos de Recompensa';
$_['column_orders']         = 'Num. Pedidos';
$_['column_total']          = 'Total ';
$_['column_action']         = 'Acción';

// Entry
$_['entry_date_start']      = 'Fecha de Inicio';
$_['entry_date_end']        = 'Fecha Final';