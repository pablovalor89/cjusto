<?php
// Heading
$_['heading_title']    = 'Informe de marketing';

// Text
$_['text_list']         = 'Lista de marketing';
$_['text_all_status']   = 'Todos los estados';

// Column
$_['column_campaign']  = 'Nombre de la campaña';
$_['column_code']      = 'Código';
$_['column_clicks']    = 'Clicks';
$_['column_orders']    = 'Num. Pedidos';
$_['column_total']     = 'Total ';

// Entry
$_['entry_date_start'] = 'Fecha de Inicio';
$_['entry_date_end']   = 'Fecha Final';
$_['entry_status']     = 'Estado del pedido';