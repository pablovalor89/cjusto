<?php
// Heading
$_['heading_title']     = 'Informe de actividades de afiliados';

// Text
$_['text_list']         = 'Lista de actividades de afiliados';
$_['text_edit']         = '<a href="affiliate_id=%d"> %s</a> ha actualizado la información de su cuenta.';
$_['text_forgotten']    = '<a href="affiliate_id=%d"> %s</a> solicitó una nueva contraseña.';
$_['text_login']        = '<a href="affiliate_id=%d"> %s</a> se ha conectado.';
$_['text_password']     = '<a href="affiliate_id=%d"> %s</a> ha actualizado la contraseña de su cuenta.';
$_['text_payment']      = '<a href="affiliate_id=%d"> %s</a> ha actualizado la información de su cuenta.';
$_['text_register']     = '<a href="affiliate_id=%d"> %s</a> ha registrado una nueva cuenta.';

// Column
$_['column_affiliate']  = 'Afiliado';
$_['column_comment']    = 'Comentario';
$_['column_ip']         = 'IP';
$_['column_date_added'] = 'Agregar Fecha';

// Entry
$_['entry_affiliate']   = 'Afiliado';
$_['entry_ip']          = 'IP';
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha Final';