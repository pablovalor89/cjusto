<?php
// Heading
$_['heading_title']     = 'Reporte de productos comprados';

// Text
$_['text_list']         = 'Lista de Productos comprados';
$_['text_all_status']   = 'Todos los estados';

// Column
$_['column_date_start'] = 'Fecha de Inicio';
$_['column_date_end']   = 'Fecha Final';
$_['column_name']       = 'Nombre del Producto';
$_['column_model']      = 'Modelo';
$_['column_quantity']   = 'Cantidad';
$_['column_total']      = 'Total ';

// Entry
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha Final';
$_['entry_status']      = 'Estado del pedido';