<?php
// Heading
$_['heading_title']     = 'Reporte de clientes en línea';

// Text
$_['text_list']         = 'Listado de clientes en línea';
$_['text_guest']        = 'Invitado';

// Column
$_['column_ip']         = 'IP';
$_['column_customer']   = 'Cliente';
$_['column_url']        = 'Última página visitada';
$_['column_referer']    = 'Referente';
$_['column_date_added'] = 'Último clic';
$_['column_action']     = 'Acción';

// Entry
$_['entry_ip']          = 'IP';
$_['entry_customer']    = 'Cliente';