<?php
// Heading
$_['heading_title']     = 'Reporte de devoluciones';

// Text
$_['text_list']         = 'Lista de devoluciones';
$_['text_year']         = 'Años';
$_['text_month']        = 'Meses';
$_['text_week']         = 'Semanas';
$_['text_day']          = 'Dias';
$_['text_all_status']   = 'Todos los estados';

// Column
$_['column_date_start'] = 'Fecha de Inicio';
$_['column_date_end']   = 'Fecha Final';
$_['column_returns']    = 'Num. Devoluciones';

// Entry
$_['entry_date_start']  = 'Fecha de Inicio';
$_['entry_date_end']    = 'Fecha Final';
$_['entry_group']       = 'Agrupar por';
$_['entry_status']      = 'Estado de la devolución';