<?php
// Heading
$_['heading_title']         = 'Reporte de Crédito del Cliente';

// Column
$_['text_list']             = 'Lista de crédito del cliente';
$_['column_customer']       = 'Nombre del cliente';
$_['column_email']          = 'E-Mail';
$_['column_customer_group'] = 'Grupo de Clientes';
$_['column_status']         = 'Estado';
$_['column_total']          = 'Total ';
$_['column_action']         = 'Acción';

// Entry
$_['entry_date_start']      = 'Fecha de Inicio';
$_['entry_date_end']        = 'Fecha Final';