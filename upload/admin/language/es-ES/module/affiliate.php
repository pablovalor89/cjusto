<?php
// Heading
$_['heading_title']    = 'Afiliado';

$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado módulo de afiliados!';
$_['text_edit']        = 'Editar módulo de afiliados';

// Entry
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo de afiliados!';