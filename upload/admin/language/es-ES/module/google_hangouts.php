<?php
// Heading
$_['heading_title']    = 'Google Hangouts';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado módulo Google Hangouts!';
$_['text_edit']        = 'Editar módulo Google Hangouts';

// Entry
$_['entry_code']       = 'Código de Google Talk';
$_['entry_status']     = 'Estado';

// Help
$_['help_code']        = 'Ir a <a href="https://developers.google.com/+/hangouts/button" target="_blank"> crear código de chatback Google Hangout</a> y copiar &amp; pegar el código generado en el cuadro de texto.';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el módulo Google Hangouts!';
$_['error_code']       = 'Código requerido';