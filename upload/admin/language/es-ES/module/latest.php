<?php
// Heading
$_['heading_title']    = 'Últimos';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Has modificado el módulo de últimos productos con éxito!';
$_['text_edit']        = 'Editar el módulo de últimos productos';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_limit']      = 'Límite';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'ATENCION: No tienes permisos para modificar el módulo de últimos productos!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Anchura requerida!';
$_['error_height']     = '¡Altura requerida!';