<?php
// Heading
$_['heading_title']    = 'Botón pagos Amazon';

$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado el módulo de pagos de Amazon!';
$_['text_edit']        = 'Editar módulo de pagos de Amazon';
$_['text_left']        = 'izquierda';
$_['text_right']       = 'Derecha';
$_['text_center']      = 'Centro';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_align']      = 'Alinear';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo de pagos de Amazon!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';