<?php
// Heading
$_['heading_title']     = 'Contenido HTML';

// Text
$_['text_module']       = 'Módulos';
$_['text_success']      = '¡Éxito: Ha modificado módulo de contenido HTML!';
$_['text_edit']         = 'Módulo de edición de HTML';

// Entry
$_['entry_name']        = 'Nombre del Módulo';
$_['entry_title']       = 'Título de encabezado';
$_['entry_description'] = 'Descripción';
$_['entry_status']      = 'Estado';

// Error
$_['error_permission']  = '¡Advertencia: No tiene permisos para modificar el módulo de contenido HTML!';
$_['error_name']        = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';