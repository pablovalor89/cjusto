<?php
// Heading
$_['heading_title']    = 'Banner';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado el módulo de Banner!';
$_['text_edit']        = 'Editar módulo de Banner';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_banner']     = 'Banner';
$_['entry_dimension']  = 'Dimensiones (W x H) y cambiar el tipo de tamaño';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo de Banner!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Anchura requerida!';
$_['error_height']     = '¡Altura requerida!';