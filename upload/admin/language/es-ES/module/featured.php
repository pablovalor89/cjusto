<?php
// Heading
$_['heading_title']    = 'Destacados';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = 'Éxito: Ha modificado módulo recomendado!';
$_['text_edit']        = 'Editar módulo recomendado';

// Entry
$_['entry_name']       = 'Nombre del Módulo';
$_['entry_product']    = 'Productos';
$_['entry_limit']      = 'Límite';
$_['entry_width']      = 'Ancho';
$_['entry_height']     = 'Alto';
$_['entry_status']     = 'Estado';

// Help
$_['help_product']     = '(Autocompletar)';

// Error
$_['error_permission'] = 'Advertencia: No tienes permiso para modificar el módulo recomendado!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']      = '¡Anchura requerida!';
$_['error_height']     = '¡Altura requerida!';