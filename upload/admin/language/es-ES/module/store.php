<?php
// Heading
$_['heading_title']    = 'Tienda';

// Text
$_['text_module']      = 'Módulos';
$_['text_success']     = '¡Ha modificado módulo \'Tienda\' con éxito!';
$_['text_edit']        = 'Editar módulo \'Tienda\'';

// Entry
$_['entry_admin']      = 'Solo usuario administrador';
$_['entry_status']     = 'Estado';

// Error
$_['error_permission'] = 'Advertencia: ¡No tienes permiso para modificar el módulo \'Tienda\'!';