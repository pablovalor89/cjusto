<?php
// Heading
$_['heading_title']     = 'Listado eBay';

// Text
$_['text_module']       = 'Módulos';
$_['text_success']      = 'Éxito: Ha modificado módulo eBay destacado!';
$_['text_edit']        	= 'Editar módulo de Ebay';
$_['text_list']         = 'Lista de Diseños';
$_['text_register']     = 'Necesita registrarse y habilitar OpenBay Pro para Ebay!';
$_['text_about'] 		= 'El módulo de visualización de eBay le permite mostrar los productos de su cuenta de eBay directamente en su sitio web.';
$_['text_latest']       = 'Últimos';
$_['text_random']       = 'Aleatorio';

// Entry
$_['entry_name']        = 'Nombre del Módulo';
$_['entry_username']    = 'Nombre de usuario de eBay';
$_['entry_keywords']    = 'Palabras clave de Búsqueda';
$_['entry_description'] = 'Incluir Descripción en Búsqueda';
$_['entry_limit']       = 'Límite';
$_['entry_length']      = 'Longitud';
$_['entry_width']       = 'Ancho';
$_['entry_height']      = 'Alto';
$_['entry_site']   		= 'Sitio eBay';
$_['entry_sort']   		= 'Ordenar por';
$_['entry_status']   	= 'Estado';

// Error
$_['error_permission']  = 'Advertencia: ¡No tiene permiso para modificar los módulos!';
$_['error_name']        = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_width']       = '¡Anchura requerida!';
$_['error_height']      = '¡Altura requerida!';