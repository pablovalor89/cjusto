<?php
// Heading
$_['heading_title']    = 'Google Base';

// Text
$_['text_feed']        = 'Fuentes';
$_['text_success']     = '¡Ha modificado correctamente el feed de Google Base!';
$_['text_edit']        = 'Editar Google Base';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_data_feed']  = 'Datos de la Url del feed';

// Error
$_['error_permission'] = '¡Advertencia: No tienes permiso para modificar el feed de Google Base!';