<?php
// Text
$_['text_subject']  = '%s - petición para resetear la contraseña';
$_['text_greeting'] = 'Se solicitó una nueva contraseña para la administración de %s.';
$_['text_change']   = 'Para restablecer su contraseña, haga clic en el enlace de abajo:';
$_['text_ip']       = 'La dirección IP utilizada para esta solicitud fue: %s';