<?php
// Text
$_['text_approve_subject']      = '%s - su cuenta de afiliado ha sido activada!';
$_['text_approve_welcome']      = '¡Bienvenido y gracias por registrarse en %s!';
$_['text_approve_login']        = 'Se ha creado su cuenta y puede iniciar sesión utilizando su dirección de correo electrónico y contraseña visitando nuestro sitio web o en la siguiente dirección:';
$_['text_approve_services']     = 'Al iniciar sesión, usted será capaz de generar códigos de rastreo, seguimiento de los pagos de comisión y editar la información de su cuenta.';
$_['text_approve_thanks']       = 'Muchas gracias,';
$_['text_transaction_subject']  = '%s - comisión de afiliado';
$_['text_transaction_received'] = 'Ha recibido %s como comisión!';
$_['text_transaction_total']    = 'El importe total de sus comisiones es de %s.';