<?php
// Text
$_['text_subject']       = '%s - Actualización de devolución %s';
$_['text_return_id']     = 'ID Devolución:';
$_['text_date_added']    = 'Fecha de devolución:';
$_['text_return_status'] = 'Su devolución ha sido actualizada al siguiente estado:';
$_['text_comment']       = 'Los comentarios para su devolución son:';
$_['text_footer']        = 'Por favor, responda a este correo electrónico si usted tiene alguna pregunta.';