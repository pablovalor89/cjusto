<?php
// Heading
$_['heading_title']       = 'Cupones';

// Text
$_['text_success']        = 'Éxito: Ha modificado los cupones!';
$_['text_list']           = 'Listado de cupones';
$_['text_add']            = 'Añadir cupón';
$_['text_edit']           = 'Editar cupón';
$_['text_percent']        = 'Porcentaje';
$_['text_amount']         = 'Cantidad Fija';

// Column
$_['column_name']         = 'Nombre de cupón';
$_['column_code']         = 'Código';
$_['column_discount']     = 'Descuento';
$_['column_date_start']   = 'Fecha de Inicio';
$_['column_date_end']     = 'Fecha Final';
$_['column_status']       = 'Estado';
$_['column_order_id']     = 'ID del Pedido';
$_['column_customer']     = 'Cliente';
$_['column_amount']       = 'Importe';
$_['column_date_added']   = 'Agregar Fecha';
$_['column_action']       = 'Acción';

// Entry
$_['entry_name']          = 'Nombre de cupón';
$_['entry_code']          = 'Código';
$_['entry_type']          = 'Tipo';
$_['entry_discount']      = 'Descuento';
$_['entry_logged']        = 'Inicio de sesión de cliente';
$_['entry_shipping']      = 'Envío gratuito';
$_['entry_total']         = 'Importe total';
$_['entry_category']      = 'Categoría';
$_['entry_product']       = 'Productos';
$_['entry_date_start']    = 'Fecha de Inicio';
$_['entry_date_end']      = 'Fecha Final';
$_['entry_uses_total']    = 'Usos por cupón';
$_['entry_uses_customer'] = 'Usos por cliente';
$_['entry_status']        = 'Estado';

// Help
$_['help_code']           = 'El código ha ser ingresao por el cliente para obtener el descuento.';
$_['help_type']           = 'Porcentaje o cantidad fija.';
$_['help_logged']         = 'El cliente debe estar conectado para utilizar el cupón.';
$_['help_total']          = 'El importe total que debe alcanzarse antes de que el cupón sea válido.';
$_['help_category']       = 'Seleccione todos los productos bajo la categoría seleccionada.';
$_['help_product']        = 'Elija productos específicos a los que se aplicará el cupón. En caso de no seleccionar ningún producto, el cupón se aplicará a toda la compra.';
$_['help_uses_total']     = 'El número máximo de veces que el cupón puede ser utilizado por cualquier cliente. Deje en blanco para uso ilimitado';
$_['help_uses_customer']  = 'El número máximo de veces que el cupón puede ser utilizado por un único cliente. Deje en blanco para uso ilimitado';

// Error
$_['error_permission']    = '¡Advertencia: No posee permisos para modificar los cupones!';
$_['error_exists']        = 'Advertencia: El código de cupón ya está en uso!';
$_['error_name']          = 'El nombre del cupón debe tener entre 3 y 128 caracteres!';
$_['error_code']          = 'El código debe ser entre 3 y 10 caracteres!';