<?php
// Heading
$_['heading_title']     = 'Seguimiento de marketing';

// Text
$_['text_success']      = 'Éxito: Se moficó el seguimiento de marketing!';
$_['text_list']         = 'Lista de seguimiento de marketing';
$_['text_add']          = 'Agregar seguimiento de Marketing';
$_['text_edit']         = 'Editar seguimiento de Marketing';

// Column
$_['column_name']       = 'Nombre de la campaña';
$_['column_code']       = 'Código';
$_['column_clicks']     = 'Clicks';
$_['column_orders']     = 'Pedidos';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre de la campaña';
$_['entry_description'] = 'Descripción de la campaña';
$_['entry_code']        = 'Código de seguimiento';
$_['entry_example']     = 'Ejemplos';
$_['entry_date_added']  = 'Agregar Fecha';

// Help
$_['help_code']         = 'El código de seguimiento que se utilizará para el seguimiento de las campañas de marketing.';
$_['help_example']      = 'Para que el sistema puede rastrear referencias usted necesita agregar el código de seguimiento al final de la URL que enlaza a su sitio.';

// Error
$_['error_permission']  = 'Advertencia: No tienes permiso para modificar seguimiento de marketing!';
$_['error_name']        = 'La campaña debe tener entre 1 y 32 caracteres!';
$_['error_code']        = 'Código de seguimiento requerido!';