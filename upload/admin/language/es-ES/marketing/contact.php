<?php
// Heading
$_['heading_title']        = 'Correo';

// Text
$_['text_success']         = 'Su mensaje se ha sido enviado correctamente!';
$_['text_sent']            = 'Su mensaje ha sido enviado con éxito a %s de %s destinatarios!';
$_['text_list']            = 'Lista de correo';
$_['text_default']         = 'Predefinido';
$_['text_newsletter']      = 'A todos los suscriptores del boletín de noticias';
$_['text_customer_all']    = 'A todos los clientes';
$_['text_customer_group']  = 'Grupo de Clientes';
$_['text_customer']        = 'Clientes';
$_['text_affiliate_all']   = 'A todos los afiliados';
$_['text_affiliate']       = 'Afiliados';
$_['text_product']         = 'Productos';

// Entry
$_['entry_store']          = 'De';
$_['entry_to']             = 'Para';
$_['entry_customer_group'] = 'Grupo de Clientes';
$_['entry_customer']       = 'Cliente';
$_['entry_affiliate']      = 'Afiliado';
$_['entry_product']        = 'Productos';
$_['entry_subject']        = 'Asunto';
$_['entry_message']        = 'Mensaje';

// Help
$_['help_customer']       = 'Autocompletar';
$_['help_affiliate']      = 'Autocompletar';
$_['help_product']        = 'Enviar sólo a los clientes que hayan pedido algún producto de la siguiente lista. (Autocompletar)';

// Error
$_['error_permission']     = 'Advertencia: No tiene permiso para enviar correos electrónicos!';
$_['error_subject']        = 'Se requiere asunto para el correo electrónico!';
$_['error_message']        = 'Se requiere mensaje para el correo electrónico!';