<?php
// Heading
$_['heading_title']     = 'Idiomas';

// Text
$_['text_success']      = '¡Ha modificado correctamente los idiomas!';
$_['text_list']         = 'Lista de Idiomas';
$_['text_add']          = 'Agregar Idioma';
$_['text_edit']         = 'Editar Idioma';

// Column
$_['column_name']       = 'Idioma';
$_['column_code']       = 'Código';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Idioma';
$_['entry_code']        = 'Código';
$_['entry_locale']      = 'Localidad';
$_['entry_image']       = 'Imagen';
$_['entry_directory']   = 'Directorio';
$_['entry_status']      = 'Estado';
$_['entry_sort_order']  = 'Ordenar por';

// Help
$_['help_code']         = 'Ejemplo: en. No cambies, si este es tu idioma por defecto.';
$_['help_locale']       = 'Ejemplo: en_US.UTF-8,en_US,en-gb,en_gb,english';
$_['help_image']        = 'Ejemplo: gb.png';
$_['help_directory']    = 'Nombre del directorio del idioma (caso sensitivo)';
$_['help_status']       = 'Ocultar/Mostrar en el menú desplegable Idioma';

// Error
$_['error_permission']  = '¡Advertencia: No tienes permiso para modificar los idiomas!';
$_['error_name']        = '¡Nombre del idioma debe ser entre 3 y 32 caracteres!';
$_['error_code']        = '¡El Código del idioma debe tener al menos 2 caracteres!';
$_['error_locale']      = '¡Configuración regional requerida!';
$_['error_image']       = '¡El nombre del archivo de la imagen debe ser entre 3 y 64 caracteres!';
$_['error_directory']   = '¡Directorio requerido!';
$_['error_default']     = '¡Advertencia: Este idioma no se puede eliminar actualmente ya que es el idioma predeterminado de la tienda!';
$_['error_admin']       = '¡Advertencia: Este idioma no se puede eliminar actualmente por que está asignado como idioma de la administración!';
$_['error_store']       = '¡Advertencia: Este idioma no se puede eliminar ya que actualmente está asignado a %s tiendas!';
$_['error_order']       = '¡Advertencia: Este idioma no se puede eliminar ya que actualmente está asignado a %s tiendas!';