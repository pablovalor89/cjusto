<?php
// Heading
$_['heading_title']      = 'Zonas Geograficas';

// Text
$_['text_success']       = '¡Ha modificado correctamente las zonas geo!';
$_['text_list']          = 'Lista de Zona Geo';
$_['text_add']           = 'Añadir zona Geográfica';
$_['text_edit']          = 'Editar Zona Geo';

// Column
$_['column_name']        = 'Nombre de la Zona Geo';
$_['column_description'] = 'Descripción';
$_['column_action']      = 'Acción';

// Entry
$_['entry_name']         = 'Nombre de la Zona Geo';
$_['entry_description']  = 'Descripción';
$_['entry_country']      = 'País';
$_['entry_zone']         = 'Zona';

// Error
$_['error_permission']   = '¡Advertencia: No tienes permiso para modificar las geo zonas!';
$_['error_name']         = '¡El nombre de la geo zona debe ser entre 3 y 32 caracteres!';
$_['error_description']  = '¡Nombre de la descripción debe ser entre 3 y 255 caracteres!';
$_['error_tax_rate']     = '¡Advertencia: Esta zona geo no se puede eliminar actualmente ya que está asignada a uno o más impuestos!';