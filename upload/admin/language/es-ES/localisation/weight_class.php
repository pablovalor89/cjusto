<?php
// Heading
$_['heading_title']    = 'Clases de peso';

// Text
$_['text_success']     = 'Éxito: Se ha modificado las clases de peso!';
$_['text_list']        = 'Lista de clases de peso';
$_['text_add']         = 'Agregar clase de peso';
$_['text_edit']        = 'Editar la clase de peso';

// Column
$_['column_title']     = 'Título de peso';
$_['column_unit']      = 'Unidad de peso';
$_['column_value']     = 'Valor';
$_['column_action']    = 'Acción';

// Entry
$_['entry_title']      = 'Título de peso';
$_['entry_unit']       = 'Unidad de peso';
$_['entry_value']      = 'Valor';

// Help
$_['help_value']       = 'Establece en 1.00000 si es su peso predeterminado.';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar clases de peso!';
$_['error_title']      = 'Título de peso debe ser entre 3 y 32 caracteres!';
$_['error_unit']       = 'Unidad de peso debe ser entre 1 y 4 caracteres!';
$_['error_default']    = 'ADVERTENCIA: Esta clase de peso no puede ser suprimida ya que actualmente se asigna como la clase de peso tienda por defecto!';
$_['error_product']    = 'ADVERTENCIA: Esta clase de peso no puede ser eliminado ya que actualmente está asignado a %s productos!';