<?php
// Heading
$_['heading_title']          = 'Zonas';

// Text
$_['text_success']           = '¡Ha modificado exitosamente las zonas!';
$_['text_list']              = 'Lista de Zonas';
$_['text_add']               = 'Añadir Zona';
$_['text_edit']              = 'Editar Zona';

// Column
$_['column_name']            = 'Nombre de Zona';
$_['column_code']            = 'Código de la Zona';
$_['column_country']         = 'País';
$_['column_action']          = 'Acción';

// Entry
$_['entry_status']           = 'Estado de la Zona';
$_['entry_name']             = 'Nombre de Zona';
$_['entry_code']             = 'Código de la Zona';
$_['entry_country']          = 'País';

// Error
$_['error_permission']       = '¡Advertencia: No tienes permiso para modificar las zonas!';
$_['error_name']             = '¡Nombre de la zona debe ser entre 3 y 128 caracteres!';
$_['error_default']          = '¡ADVERTENCIA: Esta zona no se puede eliminar actualmente ya que se asigna como la zona de almacenamiento predeterminado!';
$_['error_store']            = '¡Advertencia: Esta zona no se puede eliminar ya que actualmente está asignada a %s tiendas!';
$_['error_address']          = '¡ADVERTENCIA: Esta zona no se puede eliminar actualmente ya que se asigna a %s entradas en la libreta de direcciones!';
$_['error_affiliate']        = '¡Advertencia: Esta zona no se puede eliminar ya que actualmente está asignado a %s afiliados!';
$_['error_zone_to_geo_zone'] = '¡Advertencia: Este país no se puede eliminar actualmente ya que está asignado a %s zonas de  zonas geo!';