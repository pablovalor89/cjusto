<?php
// Heading
$_['heading_title']        = 'Monedas';

// Text
$_['text_success']         = '¡Ha modificado exitosamente las monedas!';
$_['text_list']            = 'Lista de Moneda';
$_['text_add']             = 'Añadir Moneda';
$_['text_edit']            = 'Editar Moneda';

// Column
$_['column_title']         = 'Título de Moneda';
$_['column_code']          = 'Código';
$_['column_value']         = 'Valor';
$_['column_date_modified'] = 'Última Actualización';
$_['column_action']        = 'Acción';

// Entry
$_['entry_title']          = 'Título de Moneda';
$_['entry_code']           = 'Código';
$_['entry_value']          = 'Valor';
$_['entry_symbol_left']    = 'Símbolo a la Izquierda';
$_['entry_symbol_right']   = 'Símbolo a la Derecha';
$_['entry_decimal_place']  = 'Posiciones Decimales';
$_['entry_status']         = 'Estado';

// Help
$_['help_code']            = 'No cambies, si esta es tu moneda predeterminada. Debe ser válido <a href="http://www.xe.com/iso4217.php" target="_blank"> el código ISO</a>.';
$_['help_value']           = 'Establezca en 1.00000 si esta es tu moneda predeterminada.';

// Error
$_['error_permission']     = '¡Advertencia: No tienes permiso para modificar las monedas!';
$_['error_title']          = '¡Título de moneda debe ser entre 3 y 32 caracteres!';
$_['error_code']           = '¡Código de moneda debe contener 3 caracteres!';
$_['error_default']        = '¡Advertencia: Esta moneda no se puede eliminar actualmente ya que se asigna como moneda predeterminada de la tienda!';
$_['error_store']          = '¡Advertencia: Esta moneda no se puede eliminar ya que actualmente está asignada a %s tiendas!';
$_['error_order']          = '¡Advertencia: Esta moneda no se puede eliminar ya que actualmente está asignada a %s ordenes!';