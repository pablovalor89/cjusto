<?php
// Heading
$_['heading_title']     = 'Clases de impuestos';

// Text
$_['text_success']      = 'Exito: Se ha modificado el tipo de impuesto!';
$_['text_list']         = 'Lista de tipo de impuesto';
$_['text_add']          = 'Agregar tipo de impuesto';
$_['text_edit']         = 'Editar tipo de impuesto';
$_['text_shipping']     = 'Dirección de envío';
$_['text_payment']      = 'Dirección de pago';
$_['text_store']        = 'Dirección del comercio';

// Column
$_['column_title']      = 'Nombre del impuesto';
$_['column_action']     = 'Acción';

// Entry
$_['entry_title']       = 'Nombre del impuesto';
$_['entry_description'] = 'Descripción';
$_['entry_rate']        = 'Tasa de impuesto';
$_['entry_based']       = 'Basado en';
$_['entry_geo_zone']    = 'Zona geográfica';
$_['entry_priority']    = 'Prioridad';

// Error
$_['error_permission']  = 'ADVERTENCIA: No tienes permiso para modificar el tipo de impuestos!';
$_['error_title']       = 'El tipo de impuesto debe ser entre 3 y 32 caracteres!';
$_['error_description'] = 'La descripción debe ser entre 3 y 255 caracteres!';
$_['error_product']     = 'ADVERTENCIA: Este impuesto no puede ser eliminado ya que actualmente está asignado a %s productos!';