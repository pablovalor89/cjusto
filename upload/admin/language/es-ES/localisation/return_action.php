<?php
// Heading
$_['heading_title']    = 'Devoluciones';

// Text
$_['text_success']     = 'Se han modificado las devoluciones';
$_['text_list']        = 'Volver a lista de devoluciones';
$_['text_add']         = 'Añadir nueva acción de devolución';
$_['text_edit']        = 'Editar nuevo estado de devolución';

// Column
$_['column_name']      = 'Nombre de la devolución';
$_['column_action']    = 'Acción';

// Entry
$_['entry_name']       = 'Nombre de la devolución';

// Error
$_['error_permission'] = 'Atención: No tienes permiso para modificar devoluciones.';
$_['error_name']       = 'El nombre para la devolución debe de tener de 3 a 64 caracteres!';
$_['error_return']     = 'ADVERTENCIA: Esta devolución no se puede eliminar actualmente por que esta asignado a %s productos devueltos!';