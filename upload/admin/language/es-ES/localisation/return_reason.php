<?php
// Heading
$_['heading_title']    = 'Razones de Devolución';

// Text
$_['text_success']     = 'Se han modificado las Razones de Devolución!';
$_['text_list']        = 'Lista de Diseños';
$_['text_add']         = 'Añadir nueva razón para devolución';
$_['text_edit']        = 'Editar Razones de Devolución';

// Column
$_['column_name']      = 'Motivo de la devolución';
$_['column_action']    = 'Acción';

// Entry
$_['entry_name']       = 'Motivo de la devolución';

// Error
$_['error_permission'] = 'Atención: No tiene permiso para modificar las razones de la devolución!';
$_['error_name']       = 'El motivo de la devolución debe tener entre 3 y 128 carácteres!';
$_['error_return']     = 'Atención: Este motivo de devolución no puede ser borrado ya que está actualmente asignado al %s de los productos devueltos!';