<?php
// Heading
$_['heading_title']           = 'Paises';

// Text
$_['text_success']            = '¡Ha modificado exitosamente los países!';
$_['text_list']               = 'Lista de Países';
$_['text_add']                = 'Agregar País';
$_['text_edit']               = 'Editar País';

// Column
$_['column_name']             = 'Nombre del País';
$_['column_iso_code_2']       = 'Código ISO (2)';
$_['column_iso_code_3']       = 'Código ISO (3)';
$_['column_action']           = 'Acción';

// Entry
$_['entry_name']              = 'Nombre del País';
$_['entry_iso_code_2']        = 'Código ISO (2)';
$_['entry_iso_code_3']        = 'Código ISO (3)';
$_['entry_address_format']    = 'Formato de la Dirección';
$_['entry_postcode_required'] = 'Código Postal Requerido';
$_['entry_status']            = 'Estado';

// Help
$_['help_address_format']     = 'Nombre = {firstname}<br />Apellido = {lastname}<br />Empresa = {company}<br />Dirección 1 = {address_1}<br />Dirección 2 = {address_2}<br />Ciudad = {city}<br />Código Postal = {postcode}<br />Zona = {zone}<br />Código de Zona = {zone_code}<br />País = {country}';

// Error
$_['error_permission']        = '¡Advertencia: No posee permisos para modificar los países!';
$_['error_name']              = '¡Nombre del país debe ser entre 3 y 128 caracteres!';
$_['error_default']           = '¡Advertencia: Este país no se puede eliminar actualmente ya que se asigna como el país de la tienda por defecto!';
$_['error_store']             = '¡Advertencia: Este país no se puede eliminar ya que actualmente está asignado a %s tiendas!';
$_['error_address']           = '¡Advertencia: Este país no se puede eliminar actualmente ya se asigna a %s entradas de la libreta de direcciones!';
$_['error_affiliate']         = '¡Advertencia: Este país no se puede eliminar ya que actualmente está asignado a %s afiliados!';
$_['error_zone']              = '¡Advertencia: Este país no se puede eliminar ya que actualmente está asignado a %s zonas!';
$_['error_zone_to_geo_zone']  = '¡Advertencia: Este país no se puede eliminar actualmente ya que está asignado a %s zonas de  zonas geo!';