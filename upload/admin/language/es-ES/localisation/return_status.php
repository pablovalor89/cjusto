<?php
// Heading
$_['heading_title']    = 'Estado de las Devoluciones';

// Text
$_['text_success']     = 'Éxito: Se modificó el estado de las devoluciones!';
$_['text_list']        = 'Lista de estado de las devoluciones';
$_['text_add']         = 'Añadir nuevo estado de devolución';
$_['text_edit']        = 'Editar el estado de las devoluciones';

// Column
$_['column_name']      = 'Nombre del estado de las devoluciones';
$_['column_action']    = 'Acción';

// Entry
$_['entry_name']       = 'Nombre del estado de las devoluciones';

// Error
$_['error_permission'] = 'Atención: No tiene permiso para modificar los estados de devolución!';
$_['error_name']       = 'El estado de devolución debe tener entre 3 y 32 caracteres!';
$_['error_default']    = 'Atención: Este motivo de devolución no puede ser borrado ya que está actualmente asignado como motivo de devolución por defecto!';
$_['error_return']     = 'Atención: Este estado de devolución no puede ser borrado ya que está actualmente asignado al %s de los productos devueltos!';