<?php
// Heading
$_['heading_title']      = 'Banners';

// Text
$_['text_success']       = 'Banners modificados satisfactoriamente!';
$_['text_list']          = 'Lista de Banners';
$_['text_add']           = 'Añadir Banner';
$_['text_edit']          = 'Editar Banner';
$_['text_default']       = 'Predefinido';

// Column
$_['column_name']        = 'Nombre del Banner';
$_['column_status']      = 'Estado';
$_['column_action']      = 'Acción';

// Entry
$_['entry_name']         = 'Nombre del Banner';
$_['entry_title']        = 'Título';
$_['entry_link']         = 'Vínculo';
$_['entry_image']        = 'Imagen';
$_['entry_status']       = 'Estado';
$_['entry_sort_order']   = 'Ordenar por';

// Error
$_['error_permission']   = 'Advertencia: No tienes permiso para modificar Banners!';
$_['error_name']         = 'El nombre del Banner debe estar entre 3 y 64 caracteres!';
$_['error_title']        = 'El título del Banner debe estar entre 2 y 64 caracteres!';