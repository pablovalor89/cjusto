<?php
// Heading
$_['heading_title']    = 'Cargo por pedido pequeño';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado el cargo total por pedido pequeño!';
$_['text_edit']        = 'Editar el total de cuota por pedido pequeño';

// Entry
$_['entry_total']      = 'Total del pedido';
$_['entry_fee']        = 'Cargo';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Orden de aparición';

// Help
$_['help_total']       = 'La retirada debe ser igual al total de este pedido antes de poder desactivar el total de pedido.';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el total de la cuota por pedido pequeño!';
