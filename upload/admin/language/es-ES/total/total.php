<?php
// Heading
$_['heading_title']    = 'Total ';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado totales totales!';
$_['text_edit']        = 'Editar Total Total';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar los totales totales!';