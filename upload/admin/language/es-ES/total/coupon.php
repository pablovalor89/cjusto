<?php
// Heading
$_['heading_title']    = 'Cupón';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Se ha modificado el total de cupones';
$_['text_edit']        = 'Editar cupón';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el total cupones!';