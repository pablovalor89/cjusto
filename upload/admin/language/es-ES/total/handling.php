<?php
// Heading
$_['heading_title']    = 'Cargo por manejo';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Modificar el total de cargo por manejo!';
$_['text_edit']        = 'Editar el cargo total por manejo';

// Entry
$_['entry_total']      = 'Total del pedido';
$_['entry_fee']        = 'Cargo';
$_['entry_tax_class']  = 'Tipo de Impuesto';
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Help
$_['help_total']       = 'El total que la orden debe alcanzar para que ésta orden esté activa.';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar la tarifa de manipulación total!';