<?php
// Heading
$_['heading_title']    = 'Vales de regalo';

// Text
$_['text_total']       = 'Totales del Pedido';
$_['text_success']     = 'Éxito: Ha modificado total Bono regalo!';
$_['text_edit']        = 'Modificar el total del Bono de regalo';

// Entry
$_['entry_status']     = 'Estado';
$_['entry_sort_order'] = 'Ordenar por';

// Error
$_['error_permission'] = 'ADVERTENCIA: No tienes permiso para modificar el total del Bono regalo!';