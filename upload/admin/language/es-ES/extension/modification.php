<?php
// Heading
$_['heading_title']     = 'Modificaciones';

// Text
$_['text_success']      = '¡Ha realizado las modificaciones exitosamente!';
$_['text_refresh']      = '¡Cada vez que activa/desactiva o borra una modificación necesita hacer clic en el botón Actualizar para reconstruir el caché!';
$_['text_list']         = 'Lista de Modificaciones';

// Column
$_['column_name']       = 'Nombre de Modificación';
$_['column_author']     = 'Autor';
$_['column_version']    = 'Versión';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = '¡Advertencia: No tienes permiso para cambiar las modificaciones!';