<?php
// Heading
$_['heading_title']        = 'Instalador de extensión';

// Text
$_['text_success']         = '¡Ha instalado correctamente la extensión!';
$_['text_unzip']           = '¡Extrayendo los archivos!';
$_['text_ftp']             = '¡Copiando los archivos!';
$_['text_sql']             = '¡Ejecutando SQL!';
$_['text_xml']             = '¡Aplicando modificaciones!';
$_['text_php']             = '¡Ejecutando PHP!';
$_['text_remove']          = '¡Eliminando archivos temporales!';
$_['text_clear']           = '¡Has eliminado correctamente todos los archivos temporales!';

// Entry
$_['entry_upload']         = 'Cargar Archivo';
$_['entry_overwrite']      = 'Archivos que serán sobrescritos';
$_['entry_progress']       = 'Progreso';

// Help
$_['help_upload']          = 'Requiere una modificación del archivo con la extensión ". ocmod.zip"o". ocmod.xml".';

// Error
$_['error_permission']     = '¡Advertencia: No tienes permiso para modificar las extensiones!';
$_['error_temporary']      = '¡Advertencia: Hay algunos archivos temporales que requieren ser eliminados. Haga clic en el botón Borrar para eliminarlos!';
$_['error_upload']         = '¡El archivo no puede ser cargado!';
$_['error_filetype']       = '¡Tipo de archivo inválido!';
$_['error_file']           = '¡No se pudo encontrar el archivo!';
$_['error_unzip']          = '¡No se pudo abrir el archivo zip!';
$_['error_code']           = '¡La modificación requiere un código único de identificación!';
$_['error_exists']         = '¡La modificación %s utiliza el mismo código de identificación que el que intenta establecer!';
$_['error_directory']      = '¡No se encontraron los archivos para cargar en el Directorio!';
$_['error_ftp_status']     = 'FTP necesita ser activado en la configuración';
$_['error_ftp_connection'] = 'No se pudo conectar como %s:%s';
$_['error_ftp_login']      = 'No puede iniciar sesión como %s';
$_['error_ftp_root']       = 'No se puede fijar el directorio raíz como %s';
$_['error_ftp_directory']  = 'No se puede cambiar al directorio %s';
$_['error_ftp_file']       = 'No se pudo cargar el archivo %s';
