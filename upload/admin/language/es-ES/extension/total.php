<?php
// Heading
$_['heading_title']     = 'Totales del Pedido';

// Text
$_['text_success']      = '¡Ha modificado correctamente los totales!';
$_['text_list']         = 'Lista de Ordenes de Totales';

// Column
$_['column_name']       = 'Totales del Pedido';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = '¡Advertencia: No tienes permiso para modificar los totales!';