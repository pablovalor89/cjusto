<?php
// Heading
$_['heading_title']     = 'Envío';

// Text
$_['text_success']      = '¡Ha modificado correctamente envíos!';
$_['text_list']         = 'Lista de envíos';

// Column
$_['column_name']       = 'Método de Envíos';
$_['column_status']     = 'Estado';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Error
$_['error_permission']  = '¡Adveretencia: No tienes permiso para modificar el envío!';