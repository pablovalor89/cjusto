<?php
// Heading
$_['heading_title']    = 'Módulos';

// Text
$_['text_success']     = '¡Ha modificado correctamente los módulos!';
$_['text_layout']      = '¡Después de haber instalado y configurado un módulo puede agregarlo a un diseño <a href="%s" class="alert-link"> aquí</a>!';
$_['text_add']         = 'Agregar módulo';
$_['text_list']        = 'Lista de módulos';

// Column
$_['column_name']      = 'Nombre del Módulo';
$_['column_action']    = 'Acción';

// Entry
$_['entry_code']       = 'Módulo';
$_['entry_name']       = 'Nombre del Módulo';

// Error
$_['error_permission'] = '¡Advertencia: No tienes permiso para modificar los módulos!';
$_['error_name']       = '¡El nombre del módulo debe tener entre 3 y 64 caracteres!';
$_['error_code']       = '¡Extensión requerida!';