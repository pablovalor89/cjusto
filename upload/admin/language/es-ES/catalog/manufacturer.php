<?php
// Heading
$_['heading_title']      = 'Fabricantes';

// Text
$_['text_success']       = '¡Ha modificado exitosamente los fabricantes!';
$_['text_list']          = 'Lista de Fabricantes';
$_['text_add']           = 'Agregar Fabricante';
$_['text_edit']          = 'Editar Fabricante';
$_['text_default']       = 'Predefinido';
$_['text_percent']       = 'Porcentaje';
$_['text_amount']        = 'Cantidad Fija';

// Column
$_['column_name']        = 'Nombre del Fabricante';
$_['column_sort_order']  = 'Ordenar por';
$_['column_action']      = 'Acción';

// Entry
$_['entry_name']         = 'Nombre del Fabricante';
$_['entry_store']        = 'Tiendas';
$_['entry_keyword']      = 'SEO Palabras Claves';
$_['entry_image']        = 'Imagen';
$_['entry_sort_order']   = 'Ordenar por';
$_['entry_type']         = 'Tipo';

// Help
$_['help_keyword']       = 'No utilice espacios, en su lugar utilice el guión - y asegúrese que la palabra clave es única.';

// Error
$_['error_permission']   = '¡Advertencia: No tienes permiso para modificar los fabricantes!';
$_['error_name']         = '¡El nombre del fabricante debe tener entre 2 y 64 carácteres!';
$_['error_keyword']      = '¡Palabra clave SEO ya en uso!';
$_['error_product']      = '¡Advertencia: Este fabricante no puede ser borrado porque está asignado a %s productos!';