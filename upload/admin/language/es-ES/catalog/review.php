<?php
// Heading
$_['heading_title']     = 'Comentarios';

// Text
$_['text_success']      = '¡Ha modificado exitosamente los comentarios!';
$_['text_list']         = 'Lista de Comentarios';
$_['text_add']          = 'Agregar Comentarios';
$_['text_edit']         = 'Editar Comentarios';

// Column
$_['column_product']    = 'Producto';
$_['column_author']     = 'Autor';
$_['column_rating']     = 'Calificación';
$_['column_status']     = 'Estado';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Entry
$_['entry_product']     = 'Producto';
$_['entry_author']      = 'Autor';
$_['entry_rating']      = 'Calificación';
$_['entry_status']      = 'Estado';
$_['entry_text']        = 'Texto';
$_['entry_date_added']  = 'Agregar Fecha';

// Help
$_['help_product']      = '(Autocompletar)';

// Error
$_['error_permission']  = '¡Advertencia: No tienes permiso para modificar los comentarios!';
$_['error_product']     = '¡Producto requerido!';
$_['error_author']      = '¡El autor debe tener entre 3 y 64 carácteres!';
$_['error_text']        = '¡El Texto del Comentario debe tener al menos 1 carácter!';
$_['error_rating']      = '¡Calificación del comentario requerida!';