<?php
// Heading
$_['heading_title']          = 'Atributos';

// Text
$_['text_success']           = 'Has modificado los atributos correctamente!';
$_['text_list']              = 'Lista de atributos';
$_['text_add']               = 'Agregar atributo';
$_['text_edit']              = 'Editar atributo';

// Column
$_['column_name']            = 'Nombre de atributo';
$_['column_attribute_group'] = 'Grupo de atributos';
$_['column_sort_order']      = 'Ordenar por';
$_['column_action']          = 'Acción';

// Entry
$_['entry_name']            = 'Nombre del Atributo';
$_['entry_attribute_group'] = 'Grupo de atributos';
$_['entry_sort_order']      = 'Ordenar por';

// Error
$_['error_permission']      = 'ADVERTENCIA: No tienes permiso para modificar atributos!';
$_['error_name']            = 'El nombre del atributo debe tener entre 3 y 64 caracteres.';
$_['error_product']         = 'ADVERTENCIA: Este atributo no se puede eliminar porque actualmente está asignado a los productos %s!';