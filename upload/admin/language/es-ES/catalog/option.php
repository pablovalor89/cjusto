<?php
// Heading
$_['heading_title']       = 'Opciones';

// Text
$_['text_success']        = '¡Ha modificado exitosamente las opciones!';
$_['text_list']           = 'Lista de Opciones';
$_['text_add']            = 'Agregar Opción';
$_['text_edit']           = 'Editar Opción';
$_['text_choose']         = 'Selección Única';
$_['text_select']         = 'Selección Desplegable';
$_['text_radio']          = 'Radio';
$_['text_checkbox']       = 'Checkbox';
$_['text_image']          = 'Imagen';
$_['text_input']          = 'Input';
$_['text_text']           = 'Texto';
$_['text_textarea']       = 'Área de Texto';
$_['text_file']           = 'Archivo';
$_['text_date']           = 'Fecha';
$_['text_datetime']       = 'Fecha &amp; Hora';
$_['text_time']           = 'Hora';

// Column
$_['column_name']         = 'Nombre de Opción';
$_['column_sort_order']   = 'Ordenar por';
$_['column_action']       = 'Acción';

// Entry
$_['entry_name']          = 'Nombre de Opción';
$_['entry_type']          = 'Tipo';
$_['entry_option_value']  = 'Valor de la Opción';
$_['entry_image']         = 'Imagen';
$_['entry_sort_order']    = 'Ordenar por';

// Error
$_['error_permission']    = '¡Advertencia: No tienes permiso para modificar las opciones!';
$_['error_name']          = '¡El nombre de la Opción debe tener entre 1 y 128 carácteres!';
$_['error_type']          = '¡Advertencia: Valores necesarios de la Opción!';
$_['error_option_value']  = '¡El nombre del Valor de la Opción debe tener entre 1 y 128 carácteres!';
$_['error_product']       = '¡Advertencia: Esta descarga no puede ser borrada porque está asociada a %s productos!';