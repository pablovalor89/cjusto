<?php
// Heading
$_['heading_title']     = 'Filtros';

// Text
$_['text_success']      = 'Felicidades: Has modificado los filtros!';
$_['text_list']         = 'Lista de filtros';
$_['text_add']          = 'Añadir filtro';
$_['text_edit']         = 'Editar filtro';

// Column
$_['column_group']      = 'Grupo de Filtros';
$_['column_sort_order'] = 'Ordenar por';
$_['column_action']     = 'Acción';

// Entry
$_['entry_group']       = 'Nombre del Grupo de Filtros';
$_['entry_name']        = 'Nombre del filtro';
$_['entry_sort_order']  = 'Ordenar por';

// Error
$_['error_permission']  = 'Advertencia: No tienes permiso para modificar los filtros!';
$_['error_group']       = '¡El nombre del Grupo de Filtros debe tener entre 1 y 64 carácteres!';
$_['error_name']        = 'El nombre del filtro debe tener entre 1 y 64 caracteres!';