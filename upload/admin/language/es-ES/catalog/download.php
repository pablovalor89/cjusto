<?php
// Heading
$_['heading_title']     = 'Descargas';

// Text
$_['text_success']      = '¡Tus descargas se han modificado exitosamente!';
$_['text_list']         = 'Lista de Descargas';
$_['text_add']          = 'Agregar Descarga';
$_['text_edit']         = 'Editar Descarga';
$_['text_upload']       = '¡Su archivo se ha cargado exitosamente!';

// Column
$_['column_name']       = 'Nombre de la Descarga';
$_['column_date_added'] = 'Agregar Fecha';
$_['column_action']     = 'Acción';

// Entry
$_['entry_name']        = 'Nombre de la Descarga';
$_['entry_filename']    = 'Nombre del Archivo';
$_['entry_mask']        = 'Máscara';

// Help
$_['help_filename']     = 'Puede subir utiliza el botón de subir o usando FTP para subir los archivos al directorio de descarga y luego incluir los detalles.';
$_['help_mask']         = 'Es recomendable que el nombre del archivo y la máscara sean diferentes para evitar que las personas intenten descargar directamente el archivo del link.';

// Error
$_['error_permission']  = '¡Advertencia: No tiene permiso para modificar las descargas!';
$_['error_name']        = '¡El nombre de la Descarga debe tener entre 3 y 64 carácteres!';
$_['error_upload']      = '¡Carga Requerida!';
$_['error_filename']    = '¡El nombre del archivo debe tener entre 3 y 128 carácteres!';
$_['error_exists']      = '¡El archivo no existe!';
$_['error_mask']        = '¡La máscara debe tener entre 3 y 128 carácteres!';
$_['error_filetype']    = '¡Tipo de archivo inválido!';
$_['error_product']     = '¡Advertencia: Esta descarga no puede ser borrada porque está asociada a %s productos!';