<?php
// Heading
$_['heading_title']          = 'Categorías';

// Text
$_['text_success']           = '¡Has modificado exitosamente las categorías!';
$_['text_list']              = 'Lista de categorías';
$_['text_add']               = 'Añadir Categoría';
$_['text_edit']              = 'Editar categoría';
$_['text_default']           = 'Predefinido';

// Column
$_['column_name']            = 'Nombre de Categoría';
$_['column_sort_order']      = 'Ordenar por';
$_['column_action']          = 'Acción';

// Entry
$_['entry_name']             = 'Nombre de Categoría';
$_['entry_description']      = 'Descripción';
$_['entry_meta_title'] 	     = 'Etiqueta de Meta Título';
$_['entry_meta_keyword'] 	 = 'Etiqueta de Palabras Claves';
$_['entry_meta_description'] = 'Etiqueta de Meta Descripción';
$_['entry_keyword']          = 'SEO Palabras Claves';
$_['entry_parent']           = 'Padres';
$_['entry_filter']           = 'Filtros';
$_['entry_store']            = 'Tiendas';
$_['entry_image']            = 'Imagen';
$_['entry_top']              = 'Top';
$_['entry_column']           = 'Columnas';
$_['entry_sort_order']       = 'Ordenar por';
$_['entry_status']           = 'Estado';
$_['entry_layout']           = 'Anulación de la disposición';

// Help
$_['help_filter']            = '(Autocompletar)';
$_['help_keyword']           = 'No utilice espacios, en su lugar utilice el guión - y asegúrese que la palabra clave es única.';
$_['help_top']               = 'Mostrar en la barra de menú superior. Solo funciona para las categorías padres.';
$_['help_column']            = 'Número de columnas a usar para las categorías inferiores 3. Sólo funciona para las categorías padres.';

// Error
$_['error_warning']          = '¡Advertencia: Por favor revise cuidadosamente los errores en el formulario!';
$_['error_permission']       = '¡Advertencia: No posee permisos para modificar las categorías!';
$_['error_name']             = '¡El nombre de la Categoría debe tener entre 2 y 32 carácteres!';
$_['error_meta_title']       = '¡Meta Título debe ser mayor a 3 y menor a 255 caracteres!';
$_['error_keyword']          = '¡Palabra clave SEO ya en uso!';