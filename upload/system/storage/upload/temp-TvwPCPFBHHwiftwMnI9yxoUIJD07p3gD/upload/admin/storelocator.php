<html>
<head>
<script src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
          <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=AIzaSyA3GRViajhZibChYv0YVIZZ6MuOXvmNyi8"></script>
</head>
<body>
  <div class="form-group">
    <input id="pac-input" class="form-control" style="margin-left: 20%;width: 70%;" type="text" placeholder="Search Box">
    <input id="cityLat" class="form-control" style="margin-left: 20%;width: 70%;" type="hidden" placeholder="Search Box">
		<input id="cityLng" class="form-control" style="margin-left: 20%;width: 70%;" type="hidden" placeholder="Search Box">
		
          </div>
       <div class="container" id="map-canvas" style="height:330px;"></div>
    <style>

#map-canvas {
  margin: 0;
  padding: 0;
  height: 100%;
}
    </style>
<script>

  $(document).ready(function(){
  
         function init() {
	   var map = new google.maps.Map(document.getElementById('map-canvas'), {
	     center: {
	       lat: <?php if(isset($_GET['lat']))echo $_GET['lat']; else echo '12.9715987'; ?>,
	       lng: <?php if(isset($_GET['lng']))echo $_GET['lng']; else echo '77.59456269999998'; ?>
	     },
	     zoom: 12
	   });
	

   var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
   map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
   google.maps.event.addListener(searchBox, 'places_changed', function() {
     searchBox.set('map', null);


     var places = searchBox.getPlaces();

     var bounds = new google.maps.LatLngBounds();
     var i, place;
     for (i = 0; place = places[i]; i++) {
       (function(place) {
         var marker = new google.maps.Marker({

           position: place.geometry.location
         });
         marker.bindTo('map', searchBox, 'map');
         google.maps.event.addListener(marker, 'map_changed', function() {
           if (!this.getMap()) {
             this.unbindAll();
           }
         });
         document.getElementById('cityLat').value = place.geometry.location.lat();
            document.getElementById('cityLng').value = place.geometry.location.lng();
         bounds.extend(place.geometry.location);


       }(place));
       
     }
     map.fitBounds(bounds);
     searchBox.set('map', map);
     map.setZoom(Math.min(map.getZoom(),12));

   });
   
 }
 google.maps.event.addDomListener(window, 'load', init);
          });
</script>
</body>
</html>